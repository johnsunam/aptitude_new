
import { composeWithTracker } from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {FormDb} from '../../api/form/collection/form.collection.js'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import AddPage from '../components/aptitude/page/addPage.jsx'
const composer = ( props, onData ) => {
    var clinetSubcription=Meteor.subscribe('getClient');
    var formSubcription=Meteor.subscribe('getForm');
    if(formSubcription.ready()){
      var user=window.localStorage.getItem('user')
      var a=AptitudeAdminDb.findOne({user:user});
        var clients=ClientDb.find({user:{$in:a.companies}}).fetch();
        var forms=FormDb.find({user:user}).fetch();
        let data={clients:clients,forms:forms}
        onData( null, {data} )
      }

  };


export default composeWithTracker(composer)(AddPage);
