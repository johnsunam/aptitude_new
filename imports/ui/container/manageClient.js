import { composeWithTracker } from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import ManageClient from '../components/aptitude/client/manageClient.jsx'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
const composer = ( props, onData ) => {
    var subcription=Meteor.subscribe('getClient');
    var admin = Meteor.subscribe('getAptitudeAdmin')
    if(subcription.ready()){
      let user=window.localStorage.getItem('user');
      console.log(user);
        var f=AptitudeAdminDb.findOne({user:user});
        var clients=ClientDb.find({user:{$in:f.companies}}).fetch()
        console.log(clients);
        onData( null, {clients} )
      }

  };


export default composeWithTracker(composer)(ManageClient);
