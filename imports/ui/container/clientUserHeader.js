import { composeWithTracker } from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import Header from '../components/common/clientUserHeader.jsx';
const composer = ( props, onData ) => {
let subcription=Meteor.subscribe('getClientUser');
subcription=Meteor.subscribe('getClient');
    if(subcription.ready()){
      let data;
      if(window.localStorage.getItem('appType')=="client-admin"){
         data=ClientDb.findOne({user:Meteor.userId()});
      }
      else{
        data=ClientUserDb.findOne({user:Meteor.userId()});
        console.log(data);
      }
        onData( null, {data} )
      }
      

  };


export default composeWithTracker(composer)(Header);
