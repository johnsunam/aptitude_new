import { composeWithTracker } from 'react-komposer';
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import UserList from '../components/accounts/userList/userlist.jsx'

const composer = ( props, onData ) => {
  import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
  let aptitude= Meteor.subscribe('getAptitudeAdmin');
  let client =Meteor.subscribe('getClient');
  let clientUser=Meteor.subscribe('getClientUser')
    if(clientUser.ready()){
      let users,parent;
      let id=Meteor.userId();
      switch (props.role) {
        case 'aptitude-admin':
        parent=AptitudeAdminDb.findOne({user:id});
        if(parent.admins.length!=0 || parent.client.length!=0){
          let admim=AptitudeAdminDb.find({user:{$in:parent.admins}}).fetch()
           data=admim
           window.sessionStorage.setItem('userType','aptitude-admin')
        }
        else{
          FlowRouter.go('/aptitude/add-form')
        }

          break;
          case 'client':
           let a=AptitudeAdminDb.findOne({user:id});
           parent=a?a:ClientDb.findOne({user:id})
           if(a){
             parent=a
           }
           else{
             parent=ClientDb.findOne({user:id})
             if(parent.client.length!=0 || parent.clientUser.length!=0){
               let client=ClientDb.find({user:{$in:parent.client}}).fetch()
                data=client
              window.sessionStorage.setItem('userType','client')
             }
             else{

               FlowRouter.go('/client/dashboard')
             }
           }
        break;
        case 'App-User':
        let c=ClientDb.findOne({user:id});
        let cu;
          if(c){
            console.log(c.clientUser);
               cu=ClientUserDb.find({user:{$in:c.clientUser}}).fetch()
              console.log(cu);
          }
          else {
              FlowRouter.go('/app/dashboard')
          }
         data=cu
        break;
      }
    
        onData( null,{data} )
      }

  };


export default composeWithTracker(composer)(UserList);
