import { composeWithTracker } from 'react-komposer';
import {SampleData} from '../../api/workflow/collection/sampleData.collection.js'
import Pivot from '../components/client/statistic/pivot.jsx'
const composer = ( props, onData ) => {

    var subscription=Meteor.subscribe('getSampleData');
    if(subscription.ready()){
        var data=SampleData.find().fetch();
        onData( null, {data} )
      }

  };


export default composeWithTracker(composer)(Pivot);
