import { composeWithTracker } from 'react-komposer';
import {RoleDb} from '../../api/role/collection/role.collection.js'
import ManageRole from '../components/aptitude/role/manageRole.jsx'
const composer = ( props, onData ) => {
    var subcription=Meteor.subscribe('getRole');
    if(subcription.ready()){
      let user=window.localStorage.getItem('user');
        var roles=RoleDb.find({user:user}).fetch();

        onData( null, {roles } )
      }

  };


export default composeWithTracker(composer)(ManageRole);
