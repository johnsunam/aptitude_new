import { composeWithTracker } from 'react-komposer';
import UserType from '../components/common/userType.jsx'
const composer = ( props, onData ) => {
  let subscription=Meteor.subscribe('getAccount');
  if(subscription.ready()){
    let use=props.choosedUser?props.choosedUser:window.localStorage.getItem('choosedUser');
    let user=Meteor.users.findOne({_id:use});
    let types=user.roles;
    onData(null,{types})
  }
}

export default composeWithTracker(composer)(UserType);
