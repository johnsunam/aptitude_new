import { composeWithTracker } from 'react-komposer';
import Reports from '../components/client/reports/reports.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js';

const composer = ( props, onData ) => {
  var clientUser=Meteor.subscribe('getClientUser')
  let subscription=Meteor.subscribe("getWorkFlow")
  let client=Meteor.subscribe("getClient")
  let workflowDatas=Meteor.subscribe("getWorkflowDatas");
   if(workflowDatas.ready()){
      if(window.localStorage.getItem('loginType')=="super_admin")
      {
       let client=ClientDb.findOne({user:Meteor.userId()})
      }
      else{
         let client=ClientUserDb.findOne({user:Meteor.userId()});
      }
       
        let workflow=WorkflowDb.findOne({_id:props.workflow});
        let workflowdatas=WorkflowDataDb.find({workflow:props.workflow}).fetch();
         let data={workflow:workflow,workflowdatas:workflowdatas,user:clientUser};
         
         onData( null, {data} )
  };    

}
export default composeWithTracker(composer)(Reports);
