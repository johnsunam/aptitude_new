import { composeWithTracker } from 'react-komposer';
import {TaskDb} from '../../api/task/collection/task.collection.js'
import ManageTask from '../components/aptitude/task/manageTask.jsx'
const composer = ( props, onData ) => {
    var subcription=Meteor.subscribe('getTask');
    if(subcription.ready()){
      let user=window.localStorage.getItem('user');
      var tasks=TaskDb.find({user:user}).fetch();

        onData( null, {tasks } )
      }

  };


export default composeWithTracker(composer)(ManageTask);
