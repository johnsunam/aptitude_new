import { composeWithTracker } from 'react-komposer';
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import ManageUser from '../components/aptitude/user/manageUser.jsx'
const composer = ( props, onData ) => {

    var subcription=Meteor.subscribe('getAptitudeAdmin');

    if(subcription.ready()){
      console.log('helo');
      let user=window.localStorage.getItem('user');
      var users=AptitudeAdminDb.find().fetch();
        onData( null, {users } )
      }

  };


export default composeWithTracker(composer)(ManageUser);
