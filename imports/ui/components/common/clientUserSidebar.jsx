import React,{Component} from 'react'

export default ClientUserSidebar=(props)=>{
  return(<aside className="main-sidebar">
        <section className="sidebar">
          <ul className="sidebar-menu">
            <li className="header">MAIN NAVIGATION</li>
            <li className="treeview active"> <a href="#"><i className="fa fa-dashboard"></i> <span>WorkFlows</span> <span className="pull-right-container"> <i className="fa fa-angle-left pull-right"></i> </span> </a>
            <ul>
            {props.workflows.map((workflow) => {
              return(<li><a href="" id={workflow._id}  onClick={(e)=>{
                var video = document.querySelector('video');
                video.className="hidden"
                $('#b').removeClass("show")
                $('#b').addClass("hidden")
              }}>{workflow.name}</a>
              <ul><li><a href='' id={workflow._id} onClick={(e)=>{
                FlowRouter.go(`/fillform/${e.target.id}`)
              }}>Create</a></li>
              <li><a href='' id={workflow._id} onClick={(e)=>{
                FlowRouter.go(`/view/${e.target.id}`)
              }}>View</a></li></ul></li>)
            })}
            </ul>
              </li>
          </ul>
        </section>

      </aside>)
}
