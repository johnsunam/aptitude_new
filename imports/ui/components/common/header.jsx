//header for layout

import React,{Component} from 'react'

export default Header=(props)=>{
  return(<div className="hed_top">
  <div className="mid_container">
    <div className="logo">Aptitude</div>
    <div className="hed_top_right">
      <label>{props.data.name}</label>
      <a href="#" onClick={()=>{
        window.localStorage.setItem('appType',null)
  window.localStorage.setItem('user',null)
  window.localStorage.setItem('loginType',null)
  window.localStorage.setItem('subType',null)
  Meteor.logout()
      }}><img src="../images/logout.png"/>Log Out</a></div>
  </div>
</div>)
}
