import React, {Component} from 'react';
import {FlowRouter} from 'meteor/kadira:flow-router';
import Alert from 'react-s-alert';
import commonImports from '../../common/commonImports.jsx';
import Gate from '../../../container/gate.js';
//import ChooseRole from './chooseRole.jsx';
import {Session} from 'meteor/session';
import ChooseAccount from '../../../container/chooseAccounts.js'

const animating = false,
    submitPhase1 = 1100,
    submitPhase2 = 400,
    logoutPhase1 = 800,
    $login = $(".login"),
    $app = $(".app");

export default class AdminLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showMessage: false,
            loggedIn: undefined,
            choosed: false,elements:''
        }

    }
    ripple(elem, e) {
        $(".ripple").remove();
        var elTop = elem.offset().top,
            elLeft = elem.offset().left,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var $ripple = $("<div className='ripple'></div>");
        $ripple.css({top: y, left: x});
        elem.append($ripple);
    }

    enableButton() {
        this.setState({canSubmit: true});
    }

    disableButton() {
        this.setState({canSubmit: false});
    }

    submit(e) {

        let user = e.username
        let password = e.password
        let self = this;
        console.log(user, password);

        Meteor.loginWithPassword(user, password, function(err) {
            //  Code for error check if then alert them.
            if (err) {
                Alert.warning(err.reason, {
                    position: 'top-right',
                    effect: 'bouncyflip',
                    timeout: 1000
                });
            } else {
                var now = new Date();
                var time = now.getTime();
                var expireTime = time + 1000 * 3600;
                now.setTime(expireTime);
                document.cookie = 'loginStatus=ok;expires=' + now.toGMTString();
                window.localStorage.setItem('userAccess', Meteor.user().roles);
                self.setState({loggedIn: Meteor.userId()})
                $('#userbutton').trigger('click')
            }

        })
    }
    closeModal() {
        $('#userbutton').trigger('click');
    }
    componentDidMount() {
        console.log(window.localStorage.getItem('userAccess'))
    }
    componentWillReceiveProps(nextProps){
        
         if(window.localStorage.getItem("appType")=="aptitude"){
            component=<div><button className="btn btn-primary btn-sm pull-left"
            onClick={()=>{
               // console.log(nextProps.data.aptitudeAdmin._id);
                window.localStorage.setItem('user',nextProps.data.aptitudeAdmin.user);
                this.closeModal();
                FlowRouter.go('/aptitude/add-form')
            }}
            >Admin</button>
            {nextProps.data.companies.length!=0?<button className="btn btn-primary btn-sm pull-right"
            onClick={()=>{
                $('.chooseAccess').hide();
                this.setState({choosed:true})
            }}
            >Clients</button>:<span></span>}</div>
            this.setState({elements:component});
    }
    else if (window.localStorage.getItem("appType")=="client-admin") {
        console.log(nextProps,window.localStorage.getItem("appType"));
        component=<div><button className="btn btn-primary btn-sm pull-left" onClick={()=>{
            window.localStorage.setItem('subType','client');
            this.setState({choosed:true});
            $('.chooseAccess').hide();
        }}>Admin</button>
        <button className="btn btn-primary btn-sm pull-right" onClick={()=>{
                window.localStorage.setItem('subType','App-User');
                $('.chooseAccess').hide();
                this.setState({choosed:true})
            }}>User</button>
        </div>
        this.setState({elements:component});
    } else {
        component=nextProps.clientUser.userTypes.map((type)=>{
                return(<button className="btn btn-primary btn-sm pull-left col-md-offset-2" id={type} onClick={(e)=>{
              window.localStorage.setItem('subType',e.target.id);
            this.setState({choosed:true})
            $('.chooseAccess').hide();
        }}>{type=="client"?"Admin":type}</button>)
        })
        this.setState({elements:component})
    }
    }

    render() {
        let rolestr = window.localStorage.getItem('userAccess');
        let roles = Meteor.userId()
            ? rolestr.split(',')
            : [];
       
        return (
                <div className="admin-login">
                    <div className="cont">
                        <div className="demo">
                            <div className="login">
                                <div className="logo">
                                    <center><img src="/images/original-2180-11799655.png" alt=""/></center>
                                </div>
                                <div className="login__form">
                                    <Formsy.Form ref="form" onValidSubmit={this.submit.bind(this)} id="addPage" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
                                        <div className="login__row">
                                            <svg className="login__icon name svg-icon" viewBox="0 0 20 20">
                                                <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8"/>
                                            </svg>
                                            <MyInput className="login__input name" type="text" help="Please enter the valid email or username" title="Email or username" name="username" ref="username"/>
                                            <div className="bar"></div>
                                        </div>
                                        <div className="login__row">
                                            <svg className="login__icon pass svg-icon" viewBox="0 0 20 20">
                                                <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0"/>
                                            </svg>
                                            <MyInput className="login__input pass" type="password" help="Enter the password" id="password" title="Password" name="password" ref="password"/>
                                            <div className="bar"></div>
                                        </div>
                                            <button type="submit" disabled={!this.state.canSubmit} className="login__submit">Sign in</button>
                                        <p className="login__signup">Forget Password? &nbsp;</p>
                                    </Formsy.Form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <button id='userbutton' data-toggle="modal" data-target="#chooseUser" className="hidden"></button>
                                    <div className="modal fade" id="chooseUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div className="modal-dialog" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h4 className="modal-title" id="myModalLabel">Access</h4>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div className="modal-body" style={{
                                            height: 200
                                        }}>
                                            <div className="chooseAccess">
                                                <div className="alert alert-info" style={{"fontSize": 18}}>
                                                Sytems show you have access as admin/client the app.What would you like to do?</div>
                                                     {this.state.elements}
                                                                                            </div>

                                            <div className="listedAccount">
                                                {this.state.choosed == true || window.localStorage.getItem('choosed') == 'true'
                                                    ? <ChooseAccount companies={this.props.data?this.props.data.companies:''} 
                                                    clientAdmin={this.props.clientAdmin} 
                                                    clientUser={this.props.clientUser}
                                                    closeModal={this.closeModal.bind(this)}/>
                                                    : ''}
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <Alert stack={{
                              limit: 3
                          }}/>
                </div>
            )
          }
        }