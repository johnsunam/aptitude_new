import React,{Component} from 'react';
export default class ViewReport extends Component {
  constructor(props) {
    super(props)
    this.state={fields:[]}
  }
  componentDidMount(){
    let data=this.props.formdatas[0]?this.props.formdatas[0].formdata:[];
    let keys=_.keys(data)
    this.setState({fields:keys})
  }
  componentWillReceiveProps(nextProps){
    let data=nextProps.formdatas[0]?nextProps.formdatas[0].formdata:[];
    let keys=_.keys(data)
    this.setState({fields:keys})

  }
  render(){
    console.log(this.props.formdatas)
    let list=this.props.formdatas.map((data)=>{
      console.log(data)
      let rest=_.omit(data.formdata,'images');
               let val=_.map(rest,(vl,key)=>{
                 console.log(key)
               if (vl.constructor!==Array){

                 return(<td>{vl}</td>)
 
              }
              else{
                return(<td>{vl.join()}</td>)
              }
               
        //       vl.length>1000?return(<td><img src={vl}/></td>):return(<td>{vl}</td>)

             })
             
               return(<tr>{val}</tr>)
           
    })
  	return(
      	<div className="table-responsive">
          <table className="table table-hover">
            <thead>
              <tr>
                {this.state.fields.map((field)=>{
                 if(field !='images'){
                    return(<th>{field}</th>)
                 }
       
      })}
              </tr>
            </thead>
            <tbody>
              {list}               
            </tbody>
          </table>
        </div>
  	);
  }
}