import React,{Component} from 'react';
import Alert from 'react-s-alert';
export default class FillForm extends Component {
  constructor(props) {
    super(props)
	this.state={rules:[],images:[],count:'',
showMessage:'',message:''}
  }
  formEvents(props){
	   let self=this;
     let steps=JSON.parse(props.data.workflow.flowchart);
    let pages=_.where(steps.nodeDataArray,{type:"page"});
    console.log(pages)
     let userPages=[];
	 _.map(pages,function(single){
         if(single.type=="page" ){        
            userPages.push(single)
         }	 
    })
    console.log("pagelist",userPages);
	let rules=userPages[0].formData.rules;
  let dataRules=userPages[0].formData.dataRule;
	self.setState({pages:userPages,count:0,rules:rules,dataRule:dataRules})
	let form=JSON.parse(userPages[0].formData.form);
	console.log(userPages[0].formData)
	$("#showform").formRender({
      dataType: 'json',
      formData: form
    })
	

	rules.map((rule)=>{
		console.log(rule)
		$(`#${rule.selectbox}`).parent().addClass('hidden');
      $(`#${rule.textbox}`).parent().addClass('hidden');
	  rule.camera?$('.take-picture').addClass('hidden'):''

    })
	
	 $('#showform').click(function(e){
   
      let dataRule=_.findWhere(self.state.dataRule,{parent:e.target.name,parentValue:$(`#${e.target.name} option:selected`).text()})
    console.log(dataRule)
    if(dataRule){
      let a="#"+dataRule.child
      $(a).find('option').remove().end()
      $.each(dataRule.childValue, function(index, value) {
      $(a).append($('<option>').text(value).attr('value', value));
});
    }
  
  let element=[];
  let select=[];
     if(e.target.name!=undefined){
       element=_.where(self.state.rules,{checkbox:e.target.name});
        select=_.where(self.state.rules,{select:e.target.name});
     }
    

    console.log(element)
      if(select.length>0){
        console.log('enter')
          let opt=$(`#${e.target.name}`).val()
          let rules=_.where(select,{select:e.target.name,option:opt})
          _.map(rules,function(obj){
            let box=document.getElementsByName(obj.textbox);
            let selectbox=document.getElementsByName(obj.selectbox);
              $(box).parent().removeClass('hidden');
               $(selectbox).parent().removeClass('hidden')
               obj.camera?$('.take-picture').removeClass('hidden'):'';
			   obj.camera?'':$('video').addClass('hidden');
			   obj.camera?'':$('#b').addClass('hidden');
          })
          _.map(select,function(obj){
            if(obj.option!=opt){
              let box=document.getElementsByName(obj.textbox);
              let selectbox=document.getElementsByName(obj.selectbox);
                $(box).parent().addClass('hidden');
                $(selectbox).parent().addClass('hidden')
                 obj.camera?$('.take-picture').addClass('hidden'):'';
				 obj.camera?'':$('video').addClass('hidden')
				 obj.camera?'':$('#b').addClass('hidden');
            }
          })


      }
      if(element.length>0){
_.map(element,function(obj){
      console.log(obj);
      let checkbox=document.getElementsByName(obj.checkbox);
      let box=document.getElementsByName(obj.textbox);
      let selectbox=document.getElementsByName(obj.selectbox);
      if($(checkbox).prop('checked') == true){
        obj.camera?$('.take-picture').removeClass('hidden'):'';
        $(box).parent().removeClass('hidden')
        $(selectbox).parent().removeClass('hidden')
      }
      else{
        obj.camera?$('.take-picture').addClass('hidden'):'';
          $(box).parent().addClass('hidden')
          $(selectbox).parent().addClass('hidden')
		  
		  
      }

    })
      }
    
    })
	$('.take-picture').click(function(){
      
      navigator.getUserMedia = navigator.getUserMedia ||
                 navigator.webkitGetUserMedia ||
                 navigator.mozGetUserMedia;

      var canvas = document.getElementById("c");
      var button = document.getElementById("b");
      if (navigator.getUserMedia) {
         navigator.getUserMedia({ video: { width: 1280, height: 720 } },
            function(stream) {

              button.disabled = false;
              button.className = "show btn btn-default";
              var video = document.querySelector('video');
               button.onclick = function() {
                 video.className="show"
                 canvas.getContext("2d").drawImage(video, 0, 0, 300, 300, 0, 0, 300, 300);
                 var img = canvas.toDataURL("image/png");
                 let images=self.state.images;
                 images.push(img)
                 self.setState({images:images});
                 alert("done");
                 $('.videos').addClass('hidden');
               };

               video.className="show"
               video.src = window.URL.createObjectURL(stream);
               video.onloadedmetadata = function(e) {
                 video.play();
               };
            },
            function(err) {
               console.log("The following error occurred: " + err.name);
            }
         );

      } else {
         console.log("getUserMedia not supported");
      }

    })
     if($(":submit").length==0){
        $("#showform").append('<button type="submit">submit<button/>');
      }
	//submit form data
	 $("#showform"). submit(function(e){
      e.preventDefault();
	  	
      let arr=$("#showform").serializeArray()
      console.log(arr)
	  var json=''
    var newdata={}
    var prv='';
    _.each(arr,function(single){
      
      if(prv!==single.name){
        newdata[single.name]=single.value;
      }
      else{
        let present=newdata[single.name];
       if(present.constructor===Array){
         present.push(single.value)
         newdata[single.name]=present;
       }
       else{
         newdata[single.name]=[present,single.value]
       }
      }
      prv=single.name;  
    })
    console.log(newdata)
  /*  jQuery.each(arr, function(){
    	jQuery.each(this, function(i, val){
        console.log(i,val)
    		if (i=="name") {
    			json += '"' + val + '":';
    		} else if (i=="value") {
    			json += '"' + val.replace(/"/g, '\\"') + '",';
    		}
    	});
    });
    console.log(json)
    json = "{" + json.substring(0, json.length - 1) + "}";
    console.log(json)
   let data=JSON.parse(json);
   console.log(data)*/
   let field=Object.keys(newdata);
   let vals=Object.values(newdata)
   console.log(vals)
   let datas=[];
   let c=0;
   let obj={}


   let record=_.map(vals,function(value){
     console.log(value)
     let fld=field[c];
     let label=$("label[for='"+fld+"']").text()
//     datas.push({`${fld}`:value})
     obj[label]=value
    c++
   })
   let createdAt=new Date();
   obj.images=self.state.images
   newdata.images=self.state.images
   let savedata={user:window.localStorage.getItem("user"),formdata:obj,workflow:self.props.data.workflow._id,createdAt:createdAt}
   let workflowdata={user:window.localStorage.getItem("user"),formdata:newdata,workflow:self.props.data.workflow._id,createdAt:createdAt}
   
   let workflow=self.props.data.workflow;
   if(workflow.email!=""){
      let recipients=[{
      "address": {
        "email":workflow.email.to
      }
    }]
    let CC=""
    let BCC=false;
    if(workflow.email.cc.length!=0){
        CC=workflow.email.cc.toString();
      _.map(workflow.email.cc,(single)=>{
        recipients.push({"address":{"email":single,"header_to":workflow.email.to}});
      })
    }
    if(workflow.email.bcc.length!=0){
        BCC=true;
      _.map(workflow.email.cc,(single)=>{
        recipients.push({"address":{"email":single,"header_to":workflow.email.to}});
      })
    }
 
     let mail={recipients:recipients,CC:CC,BCC:BCC,subject:workflow.email.subject,body:workflow.email.body};
    if(workflow.action!=""){
        let sel=$(`label:contains(${workflow.action.field})`);
        let id=sel[0].htmlFor;
        let val=$(`#${id}`).val();     
        if(val=='yes'){
        Meteor.call("sendEmail",mail,function(err){
         if(!err){
           console.log('mail sent')
               }
            else{
              console.log(err)
            }

          });
            }
          }
          else{
        Meteor.call("sendEmail",mail,function(err){
                if(!err){
                  console.log('mail sent')
                      }
            else{
              console.log(err)
            }

          });
          }
          }

          
      
  
  
   Meteor.call('addFormData',savedata)
   Meteor.call('addWorkflowData',workflowdata,function(err){
     if(!err){
        self.setState({showMessage:true,message:"Form submitted"})
          $('.message').addClass('su')
     }
     else{
        self.setState({showMessage:true,message:"Form Not submitted"})
          $('.message').addClass('er')
     }
   })
   self.state.image?data.image=self.state.image:''
    self.setState({formData:newdata})
    let keys=_.keys(newdata);
    let values=_.values(newdata);
     self.setState({keys:keys});
    })
  }


  componentDidMount(){
     this.formEvents(this.props);
     
}

  componentWillReceiveProps(nextProps) {
 this.formEvents(nextProps)
}
  render(){
  	return(<div>
    {this.state.showMessage?<div className="col-md-offset-10 message">{this.state.message}</div>:<span></span>}
      	<form id="showform">
    </form>
	<video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
  <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
	<canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
  <div className="images ">
  {this.state.images.length!=0?<div className="row col-md-12">
  {this.state.images.map((image)=>{
     return(<div className="col-xs-6 col-md-3">
    <a href="#" className="thumbnail">
      <img src={image} style={{height:150}} alt="..."/>
    </a>
  </div>)  })}
  
  </div>:<span></span>}
  </div>
  	</div>);
   
  }
}