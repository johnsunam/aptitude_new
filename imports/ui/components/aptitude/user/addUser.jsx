import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';

export default class addUser extends Component {
  constructor(props) {
   super(props)
   this.state={
     saveResult:false,
    edit:this.props.edit,
    user:this.props.user,
	canSubmit: false,
	res: "",
  name:'',
  dob:'',
  address:'',
  mobile:'',
  email:'',
  secQuestion:'',
  secAnswer:'',
  roleName:'',
  showMessage:'',
  message:''
   }
  }

   componentDidMount(){
     console.log(this.props.user)
     this.props.edit?this.setState({name:this.props.user.name,
    dob:this.props.user.dob,
    address:this.props.user.address,
    mobile:this.props.user.mobile,
    email:this.props.user.email,
    secQuestion:this.props.user.secQuestion,
    secAnswer:this.props.user.secAnswer,
    roleName:this.props.user.roleName}):this.setState({name:'',
    dob:'',
    address:'',
    mobile:'',
    email:'',
    secQuestion:'',
    secAnswer:'',
    roleName:''})
       }
  shouldComponentUpdate(nextProps, nextState){
    let self=this;
    Tracker.autorun(function(){
       if(Session.equals('confirm',true)){
        if(Session.get('res')==true){
          console.log('helo')
          self.setState({showMessage:true,message:"User Saved Sucessfully"})
          $('.message').addClass('su')
        }else{
           self.setState({showMessage:true,message:"User Already exits"})
          $('.message').addClass('er')
        }
              Session.set('confirm',false)
      }
    })

    return true;
}

  enableButton() {
    this.setState({ canSubmit: true });
  }
  disableButton() {
    this.setState({ canSubmit: false });
  }

  // saving user to userDb
  submit(e){
    let obj= new crudClass();
	  let name=e.name,
		dob=e.dob,
		address=this.refs.address.value,
		mobile=e.mobile,
		email=e.email,
		secQuestion=e.secQuestion,
		secAnswer=e.secAnswer,
		roleName=e.roleName;
    let user=window.localStorage.getItem('user')
    
    let record=this.props.edit?{id:this.props.user._id,data:{name:name,dob:dob,address:address,mobile:mobile,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roleName:roleName}}:
    {user:user,data:{name:name,dob:dob,address:address,mobile:mobile,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roleName:roleName}}
    let res=this.state.edit ? obj.create('editUser',record) : obj.create('createAptitudeAdmin',record);
    this.setState({saveResult:res})
    this.refs.form.reset()
    $('select').prop('selectedIndex',0);
  }


  render(){
    let user=this.props.user;
    let submitButton=<button className="btn btn-sm btn-primary" type="submit" disabled={!this.state.canSubmit} ><span>Save</span></button>
    return(<div className="col-md-10 registration_form pad_t50">
        
      <div className="col-md-8 col-md-offset-2">
	   <div className="card"></div>
        <div className="card">
        {this.state.edit?<span className="col-md-offset-9" ><a className="close" style={{paddingRight:10}} data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a></span>:<span></span>}
          <h1 className="title">{this.props.edit?"Edit User":"Add User"}</h1>
        <div className="col-md-offset-1 message">{this.state.message}</div>
          <div className="form_pad">
          <Formsy.Form ref="form" onValidSubmit={this.submit.bind(this)} id="addUser" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>

            <div className="row">
              <div className="col-md-6">
                <div className="input-container">
                  <MyInput type="text" help="Pick your username" name="name" value={this.props.edit?this.props.user.name:""} title="User Name"  ref="name" required/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                <div>Birth Date</div>
                  <MyInput type="date" help="Enter your birthdate" name="dob" value={this.props.edit?this.props.user.dob:""}    ref="dob" required/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput help="Enter your address" required placeholder="Address" name="address" title="Address" value={this.props.edit?this.props.user.dob:""} required ref="address" />
                  <div className="bar"></div>

                </div>
              </div>

              <div className="col-md-6">
                <div className="input-container">
                  <MyInput type="number" help="Enter your valid mobile number" required name="mobile" title="Mobile Number" ref="mobile" required value={this.props.edit?this.props.user.mobile:""}/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="email" help="Enter your valid email address" name="email" required title="Email ID" required value={this.props.edit?this.props.user.email:""} ref="email" />
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Enter your question" name="secQuestion" required title="Security Question" required value={this.props.edit?this.props.user.secQuestion:""} ref="secQuestion" />
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Enter the answer of the security question" required name="secAnswer" required title="Security Answer" value={this.props.edit?this.props.user.secAnswer:""} ref="secAnswer"/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Enter your role name" name="roleName" required title="Role Name" ref="roleName" value={this.props.edit?this.props.user.roleName:""}/>
                  <div className="bar"></div>
                </div>

              </div>
            </div>

            <div className="">
             {submitButton}&nbsp;&nbsp;
             <a className="btn btn-warning btn-sm" 
              onClick={()=>{
               
               this.props.edit?this.refs.form.reset(this.props.user):this.refs.form.reset();
                $('select').prop('selectedIndex',0);
              }}>Reset</a>
            </div>
            </Formsy.Form>
          </div>
        </div>
      </div>
    </div>)
  }
}
