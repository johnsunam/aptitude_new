//creates new form && previews for newly created form
import React ,{Component} from 'react';
import HTML5Backend from 'react-dnd-html5-backend';
import crudClass from '../../common/crudClass.js'
import Alert from 'react-s-alert';
var message = require('../../common/message.json');
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import FormLogic from './formLogic.jsx';
import DefaultField from './defaultField';
import SearchField from './searchKey';
import {RadioGroup, Radio} from 'react-radio-group'
import Description from './description';
import Save from './save';
var components={FormLogic,DefaultField,SearchField,Description,Save};


export default class AddForm extends Component {
  constructor(props) {
   super(props)
   this.state={
     pretab:'create-form',
     defaultFieldData:[],
     check:"",
     isChecked:false,
     step:1,
     labelName:null,
     searchKeys:null,
     formDescription:"",
     formTitle:"",
     result:"",
     messages:null,
     formData:props.form?props.form.form:null,
     formBuilder:null,
     selects:[],
     checkboxes:[],
     textboxes:[],
     rules:props.edit?props.form.rules:[],
     currentSelects:[],
     currentoptions:[],
     fields:[],
     keyfields:[],
     searchFieldData:[],selectboxes:[],selectbox:'',camera:false,selectedbox:[],parentoptions:[],childoptions:[],dataRule:[],prelist:[],
     currentComponent:'FormLogic',
     form:'',
     from:'main',
     edit:this.props.edit,
     formCredentials:props.edit?props.form:{},
     addLogic:false,
     disable:false
   }
  }

  myForm(){
    let self=this;

     $("#mainForm").formRender({
         dataType: 'json',
         formData: self.state.form
       });
      
       text=JSON.parse(self.state.form);
       labelName=[];
      for(i=0;i<text.length;i++){
        text[i].type!="button"?labelName.push({id:i,label:text[i].label,name:text[i].name,selected:false,defaultFields:false}):'';
       
      }
      
       // getting form data for implementing Logic
       let checkboxes=[],
       textboxes=[],
       selects=[];
       let checkbox=$("#mainForm input:checkbox");
       let textbox=$("#mainForm input:text");
       let textarea=$("#mainForm textarea");
       
       let select=$("#mainForm select");
       let camera=$('.take-picture')
       let count=0,b=0,a=0,t=0
       _.map(checkbox,function(single){

         checkboxes.push(checkbox[count].name)
         count++;
       })
       _.map(textbox,function(single){
         textboxes.push(textbox[b].name)
         b++;
       })
          _.map(textarea,function(single){
         textboxes.push(textarea[t].name)
         t++;
       })
       _.map(select,function(single){
         selects.push(select[a].name)
         a++
       })
     
       camera.length!=0?self.setState({camera:true}):console.log('not present');

      self.setState({checkboxes:checkboxes,
        textboxes:textboxes,selects:selects,selectboxes:selects,labelName:labelName})

         $('#save-alert').show()
        window.sessionStorage.setItem('formData', JSON.stringify(self.state.form));
         let data=JSON.stringify(self.state.form);
         let user=window.localStorage.getItem('user');
         
     
      
}
changeFrom(data){
  this.setState({from:data});
}
  componentDidMount(){
    let self=this;
   
    //events handled for the shown form
    $('#mainForm').click(function(e){
      console.log($(`#${e.target.name} option:selected`).text(),e.target.name)
      
      let dataRule=_.findWhere(self.state.dataRule,{parent:e.target.name,parentValue:$(`#${e.target.name} option:selected`).text()})  
    if(dataRule){
      let a="#"+dataRule.child
      $(a).find('option').remove().end()
      $.each(dataRule.childValue, function(index, value) {
      $(a).append($('<option>').text(value).attr('value', value));
});
    }
    let element=_.where(self.state.rules,{checkbox:e.target.name});
    let select=_.where(self.state.rules,{select:e.target.name});
      if(select.length>0){
          let opt=$(`#${e.target.name}`).val()
          let rules=_.where(select,{select:e.target.name,option:opt})
          _.map(rules,function(obj){
            let box=document.getElementsByName(obj.textbox);
            let selectbox=document.getElementsByName(obj.selectbox);
              $(box).parent().removeClass('hidden');
               $(selectbox).parent().removeClass('hidden')
               obj.camera?$('.take-picture').removeClass('hidden'):'';
          })
          _.map(select,function(obj){
            if(obj.option!=opt){
              let box=document.getElementsByName(obj.textbox);
              let selectbox=document.getElementsByName(obj.selectbox);
                $(box).parent().addClass('hidden');
                $(selectbox).parent().addClass('hidden')
                 obj.camera?$('.take-picture').addClass('hidden'):'';
            }
          })


      }
    _.map(element,function(obj){
      console.log(obj);
      let checkbox=document.getElementsByName(obj.checkbox);
      let box=document.getElementsByName(obj.textbox);
      let selectbox=document.getElementsByName(obj.selectbox);
      if($(checkbox).prop('checked') == true){
        obj.camera?$('.take-picture').removeClass('hidden'):'';
        $(box).parent().removeClass('hidden')
        $(selectbox).parent().removeClass('hidden')
      }
      else{
        obj.camera?$('.take-picture').addClass('hidden'):'';
          $(box).parent().addClass('hidden')
          $(selectbox).parent().addClass('hidden')

      }

    })
    })
  $('h4').hide();
  $('#save-alert').hide();
    var buildWrap = $(document.getElementById('fb-editors')),
    renderWrap = $(document.getElementById('fb-rendered-form')),
    fbOptions = {
      disableFields:['autocomplete','hidden','paragraph','checkbox-group'],
     dataType: 'json',
     inputSets: [
                {
         label: 'Rating',
         fields: [
         {
           type: 'number',
           subtype: 'h2',
           label: 'Rating',
           className:'rating'
         }
       ]
     },
     {
       label:'toggle button',
       fields:[
         {
           label:"choose toggle",
           type:"checkbox",
           "data-toggle":"toggle",
           "data-on":"yes",
           "data-off":"no"
         }
       ]
     },
     {
        label:"Take Picture",
        fields:[
                {
                type:'button',
                label:'Camera',
                className:'take-picture',
                id:'take-picture'
                }
               ]
      },

    ],
    typeUserEvents:{
       text:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       autocomplete:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             console.log("Done");
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       button:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       checkbox:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       'checkbox-group':{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       date:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       file:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       header:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       hidden:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       paragraph:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       number:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       'radio-group':{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       select:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

       textarea:{
         onadd:function(fld){
           $('.btn',fld).click(function(){
             $(".className-wrap").hide();
             $(".name-wrap").hide();
           });
         }
       },

     },
   formData:JSON.parse(this.state.formData)
   }
   console.log(fbOptions)
   
     formBuilder = $(buildWrap).formBuilder(fbOptions).data('formBuilder');
     this.setState({formBuilder:formBuilder})
    $('#frmb-0-save').text('Continue');
   
    $('.form-builder-save').click(function(e) {
      console.log(e.target.id)
      let obj=new crudClass();
     if(formBuilder.formData.length>2){
       buildWrap.toggle();
       renderWrap.toggle();


        //form display
       
       $("#mainForm").formRender({
         dataType: 'json',
         formData: formBuilder.formData
       });
       self.setState({form:formBuilder.formData});
      let text=JSON.parse(formBuilder.formData);
      let labelName=[];
      for(i=0;i<text.length;i++){
        text[i].type!="button"?labelName.push({id:i,label:text[i].label,name:text[i].name,selected:false,defaultFields:false}):'';
       
      }
     
       // getting form data for implementing Logic
       let checkboxes=[],
       textboxes=[],
       selects=[];
       let checkbox=$("#mainForm input:checkbox");
       let textbox=$("#mainForm input:text");
       let textarea=$("#mainForm textarea");
       
       let select=$("#mainForm select");
       let camera=$('.take-picture')
       let count=0,b=0,a=0,t=0
       _.map(checkbox,function(single){

         checkboxes.push(checkbox[count].name)
         count++;
       })
       _.map(textbox,function(single){
         textboxes.push(textbox[b].name)
         b++;
       })
          _.map(textarea,function(single){
         textboxes.push(textarea[t].name)
         t++;
       })
       _.map(select,function(single){
         selects.push(select[a].name)
         a++
       })
     
       camera.length!=0?self.setState({camera:true}):console.log('not present');

      self.setState({checkboxes:checkboxes,
        textboxes:textboxes,selects:selects,selectboxes:selects,labelName:labelName})

         $('#save-alert').show()
        window.sessionStorage.setItem('formData', JSON.stringify(formBuilder.formData));
         let data=JSON.stringify(formBuilder.formData);
         let user=window.localStorage.getItem('user');
         
        self.props.edit?self.setState({result:{id:self.props.form._id,
          data:{user:user,form:data,
            rules:self.state.rules,searchKeys:self.state.searchFieldData,
            defaultFields:self.state.defaultFieldData}}}):

        self.setState({result:{user:user,form:data,rules:self.state.rules,searchKeys:self.state.searchFieldData,defaultFields:self.state.defaultFieldData}})
       console.log(self.state.pretab)
       $(`#${self.state.pretab}`).removeClass('in active');
       $(`#${self.state.pretab}-tab`).removeClass('in active');
       if(e.target.id!='create-form'){
          $("#logic").addClass('in active');
       $("#logic-tab").addClass('in active');
       self.setState({pretab:"logic"})
       }
       else{
          $("#create-form").addClass('in active');
       $("#create-form-tab").addClass('in active');
       self.setState({pretab:"create-form"})
       }
      
       
       
     }
     else{
      $('h4').show();
     }
     $('.take-picture').click(function(){
       navigator.getUserMedia = navigator.getUserMedia ||
                 navigator.webkitGetUserMedia ||
                 navigator.mozGetUserMedia;
      var canvas = document.getElementById("c");
      var button = document.getElementById("b");
      if (navigator.getUserMedia) {
         navigator.getUserMedia({ video: { width: 1280, height: 720 } },
            function(stream) {

              button.disabled = false;
              button.className = "show btn btn-default";
              var video = document.querySelector('video');
               button.onclick = function() {
                 video.className="show"
                 canvas.getContext("2d").drawImage(video, 0, 0, 300, 300, 0, 0, 300, 300);
                 var img = canvas.toDataURL("image/png");
                 self.setState({image:img})
                 alert("done");
                 $('.videos').addClass('hidden');
               };

               video.className="show"
               video.src = window.URL.createObjectURL(stream);
               video.onloadedmetadata = function(e) {
                 video.play();
               };
            },
            function(err) {
               console.log("The following error occurred: " + err.name);
            }
         );

      } else {
         console.log("getUserMedia not supported");
      }

    })

  });
}

 changeCompnent(data){
  
   if(data=="create-form"){
     console.log(data)
     $('li #create-form').trigger('click');
     console.log($('li #create-form'))
   }
  else{
     $(`#${this.state.pretab}`).removeClass('in active');
       $(`#${this.state.pretab}-tab`).removeClass('in active');
       $(`#${data}`).addClass('in active');
       $(`#${data}-tab`).addClass('in active');
       this.setState({pretab:data})
  }
  //
  
     
 }
    defaultField(data){
      console.log(data)
      this.setState({defaultFieldData:data})
    }
    searchKey(data){
      console.log(data)
    this.setState({searchFieldData:data})
    }
    description(data){
      console.log(data)
      this.setState({formDescription:data})
    }
    addLogic(){
      this.setState({addLogic:true})
    }
    saveForm(name,description){
     
                     let obj= new crudClass()
                     this.props.edit?this.state.result.data.dataRule=this.state.dataRule:this.state.result.dataRule=this.state.dataRule;
                     this.props.edit?this.state.result.data.fields=this.state.labelName:this.state.result.fields=this.state.labelName;
                     this.props.edit?this.state.result.data.defaultFields=this.state.defaultFieldData:this.state.result.defaultFields=this.state.defaultFieldData;
                     this.props.edit?this.state.result.data.searchKeys=this.state.searchFieldData:this.state.result.searchKeys=this.state.searchFieldData;
                     this.props.edit?this.state.result.data.description=this.state.description:this.state.result.description=this.state.description;
                     this.props.edit?this.state.result.data.rules=this.state.rules:this.state.result.rules=this.state.rules; 
                     this.props.edit?this.state.result.data.name=name:this.state.result.name=name; 
                     this.props.edit?this.state.result.data.description=description:this.state.result.description=this.state.description; 
                     if(!this.props.pageform){
                       this.props.edit?obj.edit('editForm',this.state.result):obj.create('addForm',this.state.result);
                   this.setState({disable:true})
                     }
                     else{
                       console.log(this.state.result);
                       let res=this.state.result;
                       res.id=this.props.id;
                       console.log(res);
                       Meteor.call('pageForm',this.state.result);
                     }
                     
    }
 

  

  render(){
    var self=this
    return(
    <div className="col-md-10 no_pad">
      <ul className="steps_menu nav nav-tabs">
        <li className="in active" id="create-form" ><a href="#create-form" className="form-builder-save"
         id="create-form" data-toggle="tab" style={{height:50}} >
        Create form</a></li>
        <li className="" id="logic"><a className="form-builder-save" style={{height:50}}  href="#logic-tab" id="logic">Add Rule</a></li>
        <li className="" id="default"><a className=""onClick={(e)=>{
          let id=e.target.id;
          this.changeCompnent(id);
        }} style={{height:50}} href="#default-tab"  id="default" >Set Report View</a></li>
        <li className="" id="search"><a className=""onClick={(e)=>{
          let id=e.target.id;
          this.changeCompnent(id);
        }} style={{height:50}}  href="#search-tab" id="search" >Set Search Filter</a></li>
        <li className="" id="save"><a className=""onClick={(e)=>{
          let id=e.target.id;
          this.changeCompnent(id);
        }} style={{height:50}}  href="#search-tab" id="save" >Finish</a></li>
      </ul>
      <div className="tab-content">

      <div id="create-form-tab" className="tab-pane fade in active">
      <div className="row" style={{"marginTop":30,"marginBottom":20,"marginLeft":10}}>

      <div className="col-md-5 input-container">
      
      <h4 className="" style={{color:"red"}}>Please choose atleast one element</h4>
      </div>

      </div>

      <div  id="fb-editors">
      </div>
      </div>

      {/*Rules tab-content*/}
      
      <div id="logic-tab" className="tab-pane fade ">

     <div className="form-renderer">
        <div className="renderedComponent">
 
          <FormLogic changeCompnent={this.changeCompnent.bind(this)}
          stateValues={this.state} defaultField={this.defaultField.bind(this)}
          changeFrom={this.changeFrom.bind(this)}
          addLogic={this.addLogic.bind(this)}
          />
          
        </div>
       <video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
        <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
        <canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
      <div className="" id="save-alert">
      <span style={{"fontSize":20,"color":"green"}}>{this.state.message?"form sucessfully saved":""}</span>
      </div>
      </div>
      </div>
      
      {/*set default-field tab-content*/}
             
      <div  id="default-tab" className="tab-pane fade ">
     <div className="form-renderer">
        <div className="renderedComponent">
          {this.state.labelName!=null?<DefaultField changeCompnent={this.changeCompnent.bind(this)}
          stateValues={this.state} 
          defaultField={this.defaultField.bind(this)}
           myForm={this.myForm.bind(this)}
           changeFrom={this.changeFrom.bind(this)}/>:<span></span>}
        </div>
       <video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
        <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
        <canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
      <div className="" id="save-alert">
      <span style={{"fontSize":20,"color":"green"}}>{this.state.message?"form sucessfully saved":""}</span>
      </div>
      </div>
      </div>

      {/*Search tab-content*/}
      <div  id="search-tab" className="tab-pane fade ">
      <div className="form-renderer">
        <div className="renderedComponent">
          {this.state.labelName!=null?<SearchField changeCompnent={this.changeCompnent.bind(this)}
          stateValues={this.state} defaultField={this.defaultField.bind(this)}
           searchKey={this.searchKey.bind(this)}
           myForm={this.myForm.bind(this)}
           changeFrom={this.changeFrom.bind(this)}
  />:<span> </span>}
        </div>
        

       <video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
        <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
        <canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
      <div className="" id="save-alert">
      <span style={{"fontSize":20,"color":"green"}}>{this.state.message?"form sucessfully saved":""}</span>
      </div>
      </div>
      </div>

      {/* save tab-content*/}
      <div  id="save-tab" className="tab-pane fade ">
     <div className="form-renderer">
        <div className="renderedComponent">
          <Save changeCompnent={this.changeCompnent.bind(this)}
          stateValues={this.state} defaultField={this.defaultField.bind(this)}
           formDescription={this.description.bind(this)}
           saveForm={this.saveForm.bind(this)}
           myForm={this.myForm.bind(this)}
           changeFrom={this.changeFrom.bind(this)}
           />
        </div>
        

       <video className="hidden col-md-offset-4" style={{'height':200 ,'weight':200}}></video>
        <input className="hidden" id="b" type="button" disabled="true" value="Take Picture"></input><br/>
        <canvas id="c" style={{display:'none'}} width="300" height="300"></canvas>
      <div className="" id="save-alert">
      <span style={{"fontSize":20,"color":"green"}}>{this.state.message?"form sucessfully saved":""}</span>
      </div>
      </div>
      </div>
         
  <div className="main col-md-12">
            {/*Modal Starts Here*/}
  {this.state.addLogic?<div className="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 className="modal-title" id="myModalLabel">Add Logic</h4>
        </div>
        <div className="modal-body">
        <div>
        <h2>Add logic to form element</h2>
        <div className="row col-md-12">
         <div className="col-md-4">
        <h3>Choose parent Checkbox element</h3>
        <select id="checkbox" className="form-control" onChange={(e)=>{
          }}><option>choose checkbox</option>
          {this.state.checkboxes.map((single)=>{
          let label=  $("label[for='"+single+"']").text();
          label=label.trim();
          return(<option value={single}>{label}</option>)
          })}
          </select>
        </div>
        <div className="col-md-4">
        <h3>Choose textbox element</h3>
        <select id="first-textboxes" className="form-control" onChange={(e)=>{
          }}><option>choose textbox</option>
          {this.state.textboxes.map((single)=>{
          let label=  $("label[for='"+single+"']").text();
          label=label.trim();
          return(<option value={single}>{label}</option>)
          })}
          </select>
        </div>
        <div className="col-md-4">
        <h3>Choose selectbox element</h3>
        <select id="first-selectbox" className="form-control" onChange={(e)=>{
         
            this.setState({selectbox:e.target.value})
          }}>
          <option>Choose selectBox</option>
            {this.state.selectboxes.map((single)=>{
               let label=  $("label[for='"+single+"']").text();
               label=label.trim();
                 return(<option value={single}>{label}</option>)
               })}
   
          </select>

        </div>
        <div className="col-md-4">
                    {this.state.camera?<label>Camera
                                <input type="checkbox" id="first" name="camera" value="camera"/>
                            </label>:<span></span>}

        </div>
       
        </div>
        
          <div>
          <a href="#" className="btn-sm btn-primary pull-left" onClick={()=>{
            let checkbox=$("#checkbox").val();
            let textbox=$("#first-textboxes").val();
            let leftselectbox=_.without(this.state.selectboxes,this.state.selectbox)
            this.setState({selectboxes:leftselectbox})
            let  lefttext=_.without(this.state.textboxes,textbox);
            this.setState({textboxes:lefttext})
            let rules=this.state.rules;
            $('#first').is(':checked')?this.setState({camera:false}):'';
            let c=$('#first').is(':checked');
            rules.push({checkbox:checkbox,textbox:textbox,selectbox:this.state.selectbox,camera:c})
            let box= document.getElementsByName(textbox);
            let sel= document.getElementsByName(this.state.selectbox);
            c?$('.take-picture').addClass('hidden'):''
            $(box).parent().addClass('hidden');
            $(sel).parent().addClass('hidden');
            this.setState({rules:rules})
            }}>Add rule</a>
          
          </div>
          <br/>
          <br/>
          </div>
       
          <div>
          <div className="row col-md-12">
          <div className="col-md-4">
          <h3>Parent Select element</h3>
              <select id="selectbox" className="form-control" onChange={(e)=>{
                 let options=$(document.getElementsByName(e.target.value)).children();
                 this.setState({selectedbox:e.target.value})
                 let currentoptions= _.map(options,function(single){
                 return single.value
                 })
                 this.setState({currentoptions:currentoptions,currentSelects:e.target.value})
               }}>
                <option>Choose select box</option>
               {this.state.selects.map((single)=>{
                 let label=  $("label[for='"+single+"']").text();
                 label=label.trim();
                 return(<option value={single}>{label}</option>)
               })}
               </select>
          </div>
          <div className="col-md-4">
          <h3>Options of parent element</h3>
          <select id="option" className="form-control">
               <option>choose options</option>
               {this.state.currentoptions.map((single)=>{
                 let select=document.getElementsByName(this.state.currentSelects);
                let option=$(`#${this.state.currentSelects} option[value=${single}]`)
                return (<option value={option.val()}>{option.text()}</option>)
               })}
               </select></div>
          <div className="col-md-4">
          <h3>List of text fields</h3>
               <select id="second-textboxes" className="form-control" onChange={(e)=>{
               }}><option>choose textbox</option>
               {this.state.textboxes.map((single)=>{
               let label=  $("label[for='"+single+"']").text();
               label=label.trim();
               return(<option value={single}>{label}</option>)
               })}
               </select>
          </div>
          <div className="col-md-4">
          <h3> list of select elements</h3>
          
           <select id="second-selectbox" name="second-selectbox" className="form-control" onChange={(e)=>{
            this.setState({selectbox:e.target.value})
          }}>
          <option selected>Choose selectBox</option>
            {this.state.selectboxes.map((single)=>{
               let label=  $("label[for='"+single+"']").text();
               label=label.trim();
                 if(single!=this.state.selectedbox){
                   return(<option value={single}>{label}</option>)
                 }
                 
               })}
   
          </select>
          </div>
          <div className="col-md-4">
          {this.state.camera?<label>
                                <input type="checkbox" id="second" name="camera" value="camera"/>
                               Camera
                            </label>:<span></span>}
           
          </div>
          
          </div>     
          <div>
               <a href="#" className="btn-sm btn-primary pull-left" onClick={()=>{
                 let option= $('#option').val();
                 let textbox=$('#second-textboxes').val();
                 console.log($('select[name=second-selectbox]').val())
                 let leftselectbox=_.without(this.state.selectboxes,this.state.selectbox)
                this.setState({selectboxes:leftselectbox})
                 let  lefttext=_.without(this.state.textboxes,textbox);
                   this.setState({textboxes:lefttext})
                   let rules=this.state.rules;
                   $('#second').is(':checked')?this.setState({camera:false}):'';
                    let c=$('#second').is(':checked');
                   rules.push({select:this.state.currentSelects,option:option,textbox:textbox,selectbox:this.state.selectbox,camera:c})
                   let box= document.getElementsByName(textbox);
                   let sel= document.getElementsByName(this.state.selectbox);
                   $(sel).parent().addClass('hidden');
                   $(box).parent().addClass('hidden');
                   c?$('.take-picture').addClass('hidden'):console.log('helo');
                   console.log(rules)
                   this.setState({selectbox:''})
                   this.setState({rules:rules})
               }}>Add logic</a>
          
          </div>        
          </div>
         
         <div>
         
         <div className="row col-md-12">
         <h2>Add logic to form data</h2>
         <div className="col-md-5">
         <h3>Choose parent data</h3>
         <select id="parentSelect" className="form-control" onChange={(e)=>{
                 let options=$(document.getElementsByName(e.target.value)).children();
                 this.setState({selectedbox:e.target.value})
                 let currentoptions= _.map(options,function(single){
                 return $(single).text()
                 })
                 console.log(currentoptions,e.target.value)
                 this.setState({parentoptions:currentoptions,parentselect:e.target.value})
               }}>
                <option>Choose parent dropdown</option>
               {this.state.selects.map((single)=>{
                 let label=  $("label[for='"+single+"']").text();
                  label=label.trim();
                 return(<option value={single}>{label}</option>)
               })}

               </select>
         
         </div>
         <div className="col-md-5">
         <h3> choose child data</h3>
          <select id="childSelect" className="form-control" onChange={(e)=>{
                 let options=$(document.getElementsByName(e.target.value)).children();
                 this.setState({selectedbox:e.target.value})
                 let currentoptions= _.map(options,function(single){
                 return $(single).text()
                 })
                 console.log(currentoptions,e.target.value)
                 this.setState({childoptions:currentoptions,childselect:e.target.value})
               }}>
                <option>Choose child dropdown</option>
               {this.state.selects.map((single)=>{
                 let label=  $("label[for='"+single+"']").text();
                  label=label.trim();
                 return(<option value={single}>{label}</option>)
               })}

               </select>
         
         </div>
         
         </div>
               <div className="row">
               <div className="col-md-6">
               
               <h2>Parent list</h2>
               <RadioGroup
        name="parent"
        selectedValue={this.state.selectedValue}
        onChange={(data)=>{
          this.setState({parentoption:data})
        }}>
         <ul style={{"listStyleType":"none"}}>
        {this.state.parentoptions.map((single)=>{
         
           return(<li><label>
          <Radio value={single}/>{single}
        </label></li>)
         
         
        })}
         </ul>
      </RadioGroup>
    </div>
     <div className="col-md-6">
     <h2>Child list</h2>
      <CheckboxGroup
        name="child"
        value=''
        onChange={(newData)=>{
          this.setState({childList:newData})
        }}>
        <ul style={{"listStyleType":"none"}}>
           {this.state.childoptions.map((single)=>{
             return(<li> <label><Checkbox value={single}/>{single}</label></li>)
        })}
        </ul>
        
      </CheckboxGroup>
       <button className="btn btn-primary" onClick={()=>{
        let childlist=_.difference(this.state.childList,this.state.prelist)
        let dataRule={parent:this.state.parentselect,
          child:this.state.childselect,
          parentValue:this.state.parentoption,
          childValue:childlist
      }
      this.setState({prelist:this.state.childList});
      let childoptions=_.difference(this.state.childoptions,this.state.childList)
      console.log(childoptions)
      this.setState({childoptions:childoptions});
      let arr=this.state.dataRule;
      arr.push(dataRule)
      this.setState({dataRule:arr})
      }}>Add</button></div>
     
     </div>
     <div className="display_rules">
     <h2>Conditional logic list</h2>
     <ul>
      {this.state.rules.map((single,key)=>{
       let k=key;
       
      return(<li>
      
       {_.map(single,function(value,key){
         console.log($("label[for='"+value+"']").text());

         var lal=$("label[for='"+value+"']").text();
         if(value!=""){
           lal=lal==""?value:lal;
           return(<span><span>{lal}</span>&nbsp;&nbsp;</span>)
         }
         
                    
       })}
      
       <a href="#"><i className="fa fa-times" aria-hidden="true" onClick={()=>{
          let rules=self.state.rules;
          delete rules[k];
          self.setState({rules:rules});
       }}></i></a></li>)
     })}
     </ul>
    
     </div>
         </div>

        </div>
        <div className="modal-footer">
        </div>
      </div>
    </div>
  </div>:<span></span>}
         {/*Modal Ends Here*/}
  </div>
      </div>
    </div>
      )
  }
}
