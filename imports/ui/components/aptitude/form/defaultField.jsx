import React,{Component} from 'react'
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';

export default class DefaultField extends Component{
    constructor(props){
        super(props)
        this.state={
            fields:[],
            defaultFieldData:[]
        }
    }
    render(){
        console.log(this.props)
        return(<div className="slider text-center">

                <div className="row">
                <div className="">
                  <div className="col-md-11 col-md-offset-1">
                   <table className="table table-striped">
                     <thead>
                       <tr>
                       {this.state.fields.map((labelValue)=>(
                       <th>{labelValue.label}</th> 
                       ))}
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                        {this.state.fields.map((labelValue)=>(
                       <th>data</th> 
                       ))}
                       </tr>
                     </tbody>
                   </table>
                   </div>
               </div>

               <div className="row">
                <div className="content-sub">
                 <h2>Choose Default fields</h2>
                 <div className="">
                  <CheckboxGroup name="default fields"  onChange={(newroles)=>{
                    let fields=[]; 
                    let self=this;
                    _.map(newroles,function(single){
                      let field=_.findWhere(self.props.stateValues.labelName,{name:single});
                      fields.push(field);
                    })
                    this.setState({fields:fields});
                  this.setState({defaultFieldData:newroles})
                  this.props.defaultField(newroles);
                }}>
                <ul style={{"listStyleType":"none"}}>
                {this.props.stateValues.labelName.map((labelValue)=>{

                return(<li><Checkbox id="checkbox" disabled={this.props.stateValues.disable?this.props.stateValues.disable:false} value={labelValue.name}/>{labelValue.label}</li>)
                })}
                </ul>
                </CheckboxGroup>
                </div>
            </div>
                 <button className="btn btn-sm btn-primary back-btn pull-left navigate-btn" onClick={()=>{
                     this.props.defaultField(this.state.defaultFieldData);
                     this.props.changeCompnent('logic');
                 }}>Previous</button>
                 <button className="btn btn btn-sm btn-primary next-btn pull-right navigate-btn" onClick={()=>{
                     console.log(this.props)
                     
                     this.props.changeCompnent('search');
                 }} >Next </button>
                 
                </div>
              </div>

           </div>)
    }
} 