import React ,{Component} from 'react';
import crudClass from '../../common/crudClass.js';
import Alert from 'react-s-alert';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
let BpmnModeler = require('bpmn-js/lib/Modeler');
let _ = require('lodash');
let modeler;
let container;
let canvas;

const newDiagramXML =
'<?xml version="1.0" encoding="UTF-8"?>' +
'<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
    'xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" '+
    'xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" '+
    'xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" '+
    'xmlns:di="http://www.omg.org/spec/DD/20100524/DI" '+
    'xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd" '+
    'id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn">'+
  '<bpmn2:process id="Process_1" isExecutable="false">'+
    '<bpmn2:startEvent id="StartEvent_1"/>'+
  '</bpmn2:process>'+
  '<bpmndi:BPMNDiagram id="BPMNDiagram_1">'+
    '<bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">'+
      '<bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">'+
        '<dc:Bounds height="36.0" width="36.0" x="412.0" y="240.0"/>'+
      '</bpmndi:BPMNShape>'+
    '</bpmndi:BPMNPlane>'+
  '</bpmndi:BPMNDiagram>'+
'</bpmn2:definitions>';

export default class DefineWorkFlow extends Component {
 constructor(props){
  super(props);
 }



 componentDidMount(){
  container = $('#js-drop-zone');
  canvas = $('#js-canvas');
  modeler = new BpmnModeler({container:canvas});
  let downloadLink = $('#js-download-diagram');
  let downloadSvgLink = $('#js-download-svg');

  $('.buttons a').click(function(e) {
        if (!$(this).is('.active')) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

   let  setEncoded = function(link, name, data) {
        let encodedData = encodeURIComponent(data);
        if (data) {
            link.addClass('active').attr({
                'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
                'download': name
            });
        } else {
            link.removeClass('active');
        }
    };

    // check file api availability
if (!window.FileList || !window.FileReader) {
  window.alert(
    'Looks like you use an older browser that does not support drag and drop. ' +
    'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
  registerFileDrop(container, openDiagram);
}

    let exportArtifacts = _.debounce(function() {
        saveSVG(function(err, svg) {
            setEncoded(downloadSvgLink, 'diagram.svg', err ? null : svg);
        });
        saveDiagram(function(err, xml) {
            setEncoded(downloadLink, 'diagram.bpmn', err ? null : xml);
        });
    }, 500);
    modeler.on('commandStack.changed', exportArtifacts);


 }

createDiagram(event){
 event.stopPropagation();
 event.preventDefault();
 createNewDiagram();
}

 render(){
  return(
   <div className="wrapper"> 
   <div className="content" id="js-drop-zone">

    <div className="message intro">
      <div className="note">
        Drop BPMN diagram from your desktop or <a id="js-create-diagram" href>create a new diagram</a> to get started.
      </div>
    </div>

    <div className="message error">
      <div className="note">
        <p>Ooops, we could not display the BPMN 2.0 diagram.</p>

        <div className="details">
          <span>cause of the problem</span>
          <pre></pre>
        </div>
      </div>
    </div>

    <div className="canvas" id="js-canvas"></div>
  </div>

  <ul className="buttons">
    <li>
      download
    </li>
    <li>
      <a id="js-download-diagram" href title="download BPMN diagram">
        BPMN diagram
      </a>
    </li>
    <li>
      <a id="js-download-svg" href title="download as SVG image">
        SVG image
      </a>
    </li>
  </ul>
  </div>

  )
 }

}

function createNewDiagram() {
    openDiagram(newDiagramXML);
}

function openDiagram(xml) {
      modeler.importXML(xml, function(err) {
            if (err) {
                container
                    .removeClass('with-diagram')
                    .addClass('with-error');
                container.find('.error pre').text(err.message);
                console.error(err);
            } else {
                container
                    .removeClass('with-error')
                    .addClass('with-diagram');
            }
      });
}

function saveSVG(done) {
    modeler.saveSVG(done);
}

function saveDiagram(done) {
    modeler.saveXML({ format: true }, function(err, xml) {
        done(err, xml);
    });
}

function registerFileDrop(container, callback) {

  function handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();

    var files = e.dataTransfer.files;
    console.log(files);
    var file = files[0];

    var reader = new FileReader();

    reader.onload = function(e) {

      var xml = e.target.result;

      callback(xml);
    };

    reader.readAsText(file);
  }

  function handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  container.get(0).addEventListener('dragover', handleDragOver, false);
  container.get(0).addEventListener('drop', handleFileSelect, false);
}
