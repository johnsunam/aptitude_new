import React,{Component} from 'react';
import ManageWorkFlow from '../../../container/manageWorkFlow.js';

export default class ChoooseClient extends Component{
    constructor(props){
        super(props)
        this.state={
            client:props.clients[0].user
            
        }
    }
    render(){
       console.log(this.state.client)
        return( <div className="col-md-10" >
      <div className="col-md-10 col-md-offset-1">
        <h1 className="title">Manage WorkFlow</h1>
        <select className="form-control" onChange={(e)=>{
            this.setState({client:e.target.value})
        }}>
        {this.props.clients.map((single)=>{
            return(<option value={single.user}>{single.companyName}</option>)
        })}
        <option></option>
        </select>
        <ManageWorkFlow client={this.state.client}/>
            
        </div>
        </div>)
    }
}