//pages listed according to the client
import React,{Component} from 'react';
import AddPage from '../../../container/addPage.js'
import crudClass from '../../common/crudClass.js'

export default class ClientAdminPages extends Component {
  constructor(props) {
    super(props)

  }

  render(){
      console.log(this.props.workflows)
    // let pagelist=this.props.pages.map((page)=>{
    //   return(
    //     <tr>
    //   <td>{page.formName}</td>
    //   <td>{page.previewURL}</td>
    //   <td>{page.publishURL}</td>
    //   <td>{page.status}</td>
    //   <td><div className="button-container">
    //   <div className="modal fade" id={`${page._id}`} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    //   <AddPage edit="true" page={page}/>
    // </div>
    //       <a href="#" id={page._id} onClick={(e)=>{
    //     let obj=new crudClass()
    //     obj.delete('deletePage',e.target.id)
    //   }}>Delete</a></div></td>

    // </tr>)
    // })
    var count=0;
    return (
    // <div>
    // <section className="content-header">
    //  <h1>Client Pages </h1>
    // </section>
    // <section className="content">
    // <table width="100%" border="0" cellspacing="0" cellpadding="0" className="table_cont">
    //           <tr>
    //             <th>Form Name</th>
    // 			<th> Preview URL </th>
    // 			<th> Publish URL </th>
    // 			<th>Status</th>
    //             <th>Access</th>
    //            </tr>
    //            {pagelist}
    //   </table>
    //   </section>
    //   </div>
    <div className="row">
    <div className="col-md-12">
      <h1>
          Dashboard
          <small>Control panel</small>
        </h1>
        <div className="box">
         <div className="box-header with-border">
            <h3 className="box-title">Workflows</h3>
            <div className="box-body">
            <div className="row mt25">
             {this.props.workflows.map((workflow)=>{

               count=count+1;
          return(<div className="col-lg-3">
                  <div className="small-box bg-primary">
                    <div className="inner">
                      <h3>{count}</h3>

                      <p><b>{workflow.name}</b></p>
                      <p>{workflow.description}</p>
                    </div>
                    <div className="icon">
                      <i className="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" id={workflow._id} onClick={(e)=>{
            FlowRouter.go(`/reports/${e.target.id}`)
          }} className="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>

              </div>

            )
        })}
            </div>
            </div>
          </div>
        </div>
        {/*<div className="btnCustom">
        {this.props.workflows.map((workflow)=>{
          return(<label id={workflow._id} className="btn workflow_button"  
          onClick={(e)=>{
            FlowRouter.go(`/reports/${e.target.id}`)
          }}
           role="button"><strong id={workflow._id}>{workflow.name}</strong></label>)
        })}
          
        </div>*/}

     </div>
     </div>
)

  }
}