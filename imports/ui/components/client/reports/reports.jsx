import React,{Component} from 'react';
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import moment from '../../../../../public/lib/moment-2.13.0'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
 import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {PieChart, Pie } from 'recharts';
import Charts from './charts';

export default class ClientAdminPages extends Component {
  constructor(props) {
    super(props)
    this.state={table_fields:[],defaultFields:[],fields:[],current:[],rowdata:[],type:null,
      workflowdatas:props.data.workflowdatas,t:'',rules:[],image:'',count:'',dataId:'',images:[],datas:[],
    choosedSearch:[],searchFields:[],defaultsData:[],newfields:[],otherfields:[],defield:'',dateData:[],roomData:[],data1:[],data2:[],data3:[],data4:[]}
  }


//search query
  searchForm(arr){
    var newfields=[]
    console.log(arr)
    if(arr[0].value!=="" && arr[1].value!==""){
       _.map(this.state.datas,function(single){
      if(arr[0].value!=="" && arr[1].value==""){
        if(moment(single.createdAt).isSameOrAfter(arr[0].value)){
           newfields.push(single)
          }
     }
      else if(arr[0].value==="" && arr[1].value!=="" ){
        if(moment(single.createdAt).isSameOrBefore(arr[1].value))
        newfields.push(single)
      }    
      else{
        if(moment(single.createdAt).isSameOrAfter(arr[0].value) && moment(single.createdAt).isSameOrBefore(arr[1].value))
        newfields.push(single)
      }
   })
  }
else{
  newfields=this.state.datas;
}
     _.each(arr,function(obj){
       var tem=[]
      if(obj.value!=="" && obj.name!=='from' && obj.name!=='to'){

          _.each(newfields,function(single){
            single[obj.name]==obj.value?tem.push(single):'';
          })
          newfields=tem;
      }
    })
   
   this.setState({newfields:newfields});
  }


  table_fields(){
    let self=this;
    
    let forms=JSON.parse(this.props.data.workflow.flowchart);
    let table_fields=[]
    let fields=[];
    _.map(forms.nodeDataArray,function(single){
      if(single.type=="page"){
        fields=fields.concat(single.formData.fields);
        _.map(single.formData.defaultFields,function(field){
          let label=_.findWhere(single.formData.fields,{name:field})
          table_fields.push(label);
        })
      }
    })
    let current=_.map(table_fields,function(single){
              return single.label
          })
    let rowdata=[];   
    this.setState({table_fields:current,defaultFields:table_fields,fields:fields});

      if(this.state.type=='daily'||this.state.type==null){
        console.log(self.state.workflowdatas)
        let start=moment().format('DD')
        let countData=[]
          for(i=0;i<=start;i++){
            let c=0;
            let date=moment().startOf('month').add(i,'d').toDate();
            date=String(date)        
            let d=_.map(self.state.workflowdatas,function(single){
              let s=moment(single.createdAt).startOf('day').toDate();
              s=String(s);
            //  console.log(moment(single.createdAt).startOf('day').toDate(),date)
           // moment(single.createdAt).startOf('day').toDate()==date?c++:""
            if(s==date){
              c++
            }
            
            })
           countData.push([i,c])
            
          }
          let t={countData:countData,field:["Day","Count"]}
          this.setState({t:t});
      }

      if(this.state.type=='weekly'){
        let start=moment().format('DD')
        let countData=[]
        let n=0
        let i;
          for(i=0;i<=start;){
            n++;
            let c=0;
            let date=moment().startOf('month').add(i,'d').toDate();
            date=String(date)
                        
            let d=_.map(self.state.workflowdatas,function(single){
              let s=moment(single.createdAt).startOf('day').toDate();
              s=String(s);
            if(s==date){
              c++
            }
            
            })
           countData.push([n,c])
            i=i+7;
          }
          
          if(start-(i-7)>0){
            let c=0;
            for( j=i-7+1;j<=start;j++){
            
            let date=moment().startOf('month').add(j,'d').toDate();
            date=String(date)           
            let d=_.map(self.state.workflowdatas,function(single){
              let s=moment(single.createdAt).startOf('day').toDate();
              s=String(s);
            //  console.log(moment(single.createdAt).startOf('day').toDate(),date)
           // moment(single.createdAt).startOf('day').toDate()==date?c++:""
            if(s==date){
              c++
            }
            })
            
          }
          countData.pop()
          countData.push([n,c])
          }
          let t={countData:countData,field:["Week","Count"]}
          this.setState({t:t});
      }

      if(this.state.type=='monthly'){
        let start=moment().dayOfYear()
        let countData=[]
        let n=0
        let i;
          for(i=0;i<=start;){
            n++;
            let c=0;
            let date=moment().startOf('year').add(i,'d').toDate();
            date=String(date)
                        
            let d=_.map(self.state.workflowdatas,function(single){
              let s=moment(single.createdAt).startOf('day').toDate();
              s=String(s);
            //  console.log(moment(single.createdAt).startOf('day').toDate(),date)
           // moment(single.createdAt).startOf('day').toDate()==date?c++:""
            if(s==date){
              c++
            }
            
            })
           countData.push([n,c])

            i=i+30;
          }
          
          if(start-(i-30)>0){ 
            let c=0;
            for( j=i-30+1;j<=start;j++){
            let date=moment().startOf('year').add(j,'d').toDate();
            date=String(date)           
            let d=_.map(self.state.workflowdatas,function(single){
              let s=moment(single.createdAt).startOf('day').toDate();
              s=String(s);
            //  console.log(moment(single.createdAt).startOf('day').toDate(),date)
           // moment(single.createdAt).startOf('day').toDate()==date?c++:""
            if(s==date){
              console.log('en')
              c++
            }
            })
            
          }
          countData.pop()
          countData.push([n,c])
          }

          let t={countData:countData,field:["Month","Count"]}
          this.setState({t:t});
      }

    self.state.workflowdatas.map((single)=>{
      let formdata=[]; 
     table_fields.map((c)=>{
      formdata.push(single.formdata[c.name])   
   })
   formdata.push(single._id);
     rowdata.push(formdata)
    })
    this.setState({rowdata:rowdata})
  }

  formEvents(props){

	   let self=this;
     let steps=JSON.parse(props.data.workflow.flowchart);
    let pages=_.where(steps.nodeDataArray,{type:"page"});
    
     let userPages=[];
	 _.map(pages,function(single){
         let roles=_.intersection(single.roles,props.data.user.roles)
         if(single.type=="page" && roles.length!=0){

            userPages.push(single)
         }	 
    })
	let rules=pages[0].formData.rules;
  let dataRules=pages[0].formData.dataRule;
	self.setState({pages:pages,count:0,rules:rules,dataRule:dataRules})
	let form=JSON.parse(pages[0].formData.form);
  
	$("#showform").formRender({
      dataType: 'json',
      formData: form
    })

	$(".button-input").addClass('hidden');
 
  
	rules.map((rule)=>{
    console.log(rule)
    if(rule!=null){
      $(`#${rule.selectbox}`).parent().addClass('hidden');
      $(`#${rule.textbox}`).parent().addClass('hidden');
	  rule.camera?$('.take-picture').addClass('hidden'):''
    }
    })

 	 $('#showform').click(function(e){
      let dataRule=_.findWhere(self.state.dataRule,{parent:e.target.name,parentValue:$(`#${e.target.name} option:selected`).text()})
    if(dataRule){
      let a="#"+dataRule.child
      $(a).find('option').remove().end()
      $.each(dataRule.childValue, function(index, value) {
      $(a).append($('<option>').text(value).attr('value', value));
});
    }
    let element=_.where(self.state.rules,{checkbox:e.target.name});
    let select=_.where(self.state.rules,{select:e.target.name});
      if(select.length>0){
          let opt=$(`#${e.target.name}`).val()
          let rules=_.where(select,{select:e.target.name,option:opt})
          _.map(rules,function(obj){
            console.log(obj)
            let box=document.getElementsByName(obj.textbox);
            let selectbox=document.getElementsByName(obj.selectbox);
              $(box).parent().removeClass('hidden');
               $(selectbox).parent().removeClass('hidden')
               obj.camera?$('.take-picture').removeClass('hidden'):'';
			   obj.camera?'':$('video').addClass('hidden');
			   obj.camera?'':$('#b').addClass('hidden');
          })
          _.map(select,function(obj){
            if(obj.option!=opt){
              let box=document.getElementsByName(obj.textbox);
              let selectbox=document.getElementsByName(obj.selectbox);
                $(box).parent().addClass('hidden');
                $(selectbox).parent().addClass('hidden')
                 obj.camera?$('.take-picture').addClass('hidden'):'';
				 obj.camera?'':$('video').addClass('hidden')
				 obj.camera?'':$('#b').addClass('hidden');
            }
          })


      }
    _.map(element,function(obj){
      console.log(obj)
      let checkbox=document.getElementsByName(obj.checkbox);
      let box=document.getElementsByName(obj.textbox);
      let selectbox=document.getElementsByName(obj.selectbox);
      if($(checkbox).prop('checked') == true){
        obj.camera?$('.take-picture').removeClass('hidden'):'';
        $(box).parent().removeClass('hidden')
        $(selectbox).parent().removeClass('hidden')
      }
      else{
        obj.camera?$('.take-picture').addClass('hidden'):'';
          $(box).parent().addClass('hidden')
          $(selectbox).parent().addClass('hidden')
		  
		  
      }

    })
    })

    
	$('.take-picture').click(function(){
      
      navigator.getUserMedia = navigator.getUserMedia ||
                 navigator.webkitGetUserMedia ||
                 navigator.mozGetUserMedia;

      var canvas = document.getElementById("c");
      var button = document.getElementById("b");
      if (navigator.getUserMedia) {
         navigator.getUserMedia({ video: { width: 1280, height: 720 } },
            function(stream) {

              button.disabled = false;
              button.className = "show btn btn-default";
              var video = document.querySelector('video');
               button.onclick = function() {
                 video.className="show"
                 canvas.getContext("2d").drawImage(video, 0, 0, 300, 300, 0, 0, 300, 300);
                 var img = canvas.toDataURL("image/png");
                 self.setState({image:img})
                 alert("done");
                 $('.videos').addClass('hidden');
               };

               video.className="show"
               video.src = window.URL.createObjectURL(stream);
               video.onloadedmetadata = function(e) {
                 video.play();
               };
            },
            function(err) {
               console.log("The following error occurred: " + err.name);
            }
         );

      } else {
         console.log("getUserMedia not supported");
      }

    })

	//submit form data
	 $("#addRemark").click(function(e){
      e.preventDefault();
	  	console.log('clicked')
      let arr=$("#showform").serializeArray()
	  var json=''
    jQuery.each(arr, function(){
    	jQuery.each(this, function(i, val){
    		if (i=="name") {
    			json += '"' + val + '":';
    		} else if (i=="value") {
    			json += '"' + val.replace(/"/g, '\\"') + '",';
    		}
    	});
    });
    json = "{" + json.substring(0, json.length - 1) + "}";
   let data=JSON.parse(json);
   let field=Object.keys(data);
  
   let vals=Object.values(data)
   let datas=[];
   let c=0;
   let obj={}
   

   let record=_.map(vals,function(value){
     let fld=field[c];
     let label=$("label[for='"+fld+"']").text()
//     datas.push({`${fld}`:value})
     obj[label]=value
    c++
   })
   let createdAt=new Date();
   obj.image=self.state.image
   obj.remark=$('#remark').val()
   data.image=self.state.image
   data.remark=$('#remark').val()
   console.log(self.state.dataId)
   let savedata={formdata:obj,id:self.state.dataId}
   let workflowdata={formdata:data,id:self.state.dataId}
    
   Meteor.call('updateFormData',savedata)
   Meteor.call('updateWorkflowData',workflowdata)
   self.state.image?data.image=self.state.image:''
   
   
    self.setState({formData:data})
    let keys=_.keys(data);
    let values=_.values(data);
     self.setState({keys:keys});
    })
  }


 //defaults field
 defaultsData(){
var data=_.map(this.state.workflowdatas,function(single){
    var d=single.formdata;
    d.incidentId=single._id;
      return d;
    })
     var forms=JSON.parse(this.props.data.workflow.flowchart);
    var table_fields=[]
    var defaults=[];
    var fields=[];
    _.map(forms.nodeDataArray,function(single){
      if(single.type=="page"){
        console.log(single)
        fields=fields.concat(single.formData.fields);

        _.map(single.formData.defaultFields,function(field){
          console.log(field)
           d=_.findWhere(single.formData.fields,{name:field})
           d!=undefined?defaults.push(_.findWhere(single.formData.fields,{name:field})):''
          
        })
      }
    })
 //   defaults=defaults==0?fields:defaults;
    var d=_.map(defaults,function(single){
      return single.label;
    })
      d.push("incidentId");
      this.setState({defield:d});
     var datas= _.map(data,function(obj){
        var d={};
          _.map(obj,function(value,key){
            if(key!="images"){
              let field=_.findWhere(defaults,{name:key});
              field==undefined?d.incidentId=value:d[field.label]=value;
            }
          })

          return d;
      })

        var df=datas[0]?_.keys(datas[0]):[];
        this.setState({newfields:datas});
      this.setState({defaultsData:datas}); 
      this.setState({perfields:datas});
      
}

//get datas function
getDatas(){
  var self=this;
  var data=_.map(this.state.workflowdatas,function(single){
    console.log(single)
     var d=single.formdata;
    d.createdAt=moment(single.createdAt).format('YYYY-MM-DD')
    d.incidentId=single._id;
    console.log(d)
      return d;
    })
     var forms=JSON.parse(this.props.data.workflow.flowchart);
     console.log(forms)
    var table_fields=[]
    var fields=[];
    var searchFields=[]
    _.map(forms.nodeDataArray,function(single){
      if(single.type=="page"){
        fields=fields.concat(single.formData.fields);
        //self.searchFields=single.formData.searchFields;
        //self.setState({searchFields:single.formData.searchFields});
       _.map(single.formData.searchKeys,function(search){
          var field=_.findWhere(single.formData.fields,{name:search})
          searchFields.push(field);

       })
  //  console.log(searchFields)
       self.setState({searchFields:searchFields});
        _.map(single.formData.defaultFields,function(field){
          var label=_.findWhere(single.formData.fields,{name:field})
          table_fields.push(label);
        })


      }
    })
      
     var datas= _.map(data,function(obj){
        var d={};
        console.log(obj)
          _.map(obj,function(value,key){

            if(key!="images"){
              console.log(fields)
              let field=_.findWhere(fields,{name:key});
              if(key=="createdAt" || key=="incidentId"){
                 key=="incidentId"?d.incidentId=value:d.createdAt=value;
              }
              else{
               
                d[field.label]=value
              }
             }
             
          })
          return d;
      })
      console.log(datas)
      this.setState({datas:datas});  
}

addFields(fields){
   var newf=[];     
   var allDatas=this.state.datas;
            _.map(allDatas,function(data){
              let st={};
               _.map(fields,function(field){  
                 st[field]=data[field];
          })
          newf.push(st);
        })

    this.setState({newfields:newf});
}
//end of getDatas function
componentWillMount(){
this.getDatas();
this.defaultsData();

}


otherDatas(){
var all=_.keys(this.state.datas[0]);
var defaults=_.keys(this.state.defaultsData[0]);
var others=_.difference(all,defaults);
console.log(others)
this.setState({otherfields:others})
}


/*barChartData(){
  let self=this;
  let roomData = [];
  let dateData=[];
  let data1=[];
  let data2=[]
  let data3=[];
  let data4=[];
if(_.has(this.state.datas[0],"Need maintenance")){
   
  let rooms=_.pluck(this.state.datas,'Room No.');
  console.log(rooms)
    let orgRooms=_.uniq(rooms);
console.log(orgRooms)
  _.each(orgRooms,function(num){
    console.log(num)
    let romData=_.where(self.state.datas,{"Room No":num});
    let romcount=_.countBy(romData,function(single){
       var x=single["Need maintenance"].toLowerCase();  
      return x==="yes"?'yes':'no';
    })
    data1.push({name:num,value:romcount.yes});
    data2.push({name:num,value:romcount.no});
    roomData.push({room:num,yes:romcount.yes,no:romcount.no,amt:romcount.yes+romcount.no})
    }) 
  //  console.log(roomData)
      this.setState({roomData:roomData})
      this.setState({data1:data1});
      this.setState({data2:data2})

   //dateData\
   
    let dates=_.pluck(self.state.datas,'createdAt');
    let orgdates=_.uniq(dates);
    
    _.each(orgdates,function(num){

    let date=_.where(self.state.datas,{"createdAt":num});
    let dateCount=_.countBy(date,function(single){
    var x=single["Need maintenance"].toLowerCase();                                                                                                                                 
      return x==="yes"?'yes':'no';
    })
    data3.push({name:num,value:dateCount.yes})
    data4.push({name:num,value:dateCount.no});
    dateData.push({date:num,yes:dateCount.yes,no:dateCount.no,amt:dateCount.yes+dateCount.no});
    })
    console.log(dateData)
    this.setState({dateData:dateData,data3:data3,data4:data4});
}
}*/


  componentDidMount(){
    this.addFields(this.state.defield);
  //  this.barChartData();
    var data=_.map(this.props.data.workflowdatas,function(single){
      return single.formdata
    })
    this.table_fields();
    var js=JSON.parse(this.props.data.workflow.flowchart)
            var page=_.findWhere(js.nodeDataArray,{type:"page"})
            
      $("#showform").formRender({
      dataType: 'json',
      formData:JSON.parse(page.formData.form)
    })
    this.formEvents(this.props);
    this.otherDatas()
    
  }

  componentWillReceiveProps(nextProps) {
 this.formEvents(nextProps)
}

  render(){
  
  	return(<div className="row">
        <div class="col-md-12">
        <h1>
          Workflow
          <small>Control panel</small>
        </h1>
        <div className="box">
        <div className="box-header with-border">
            <h3 className="box-title">Monthly Recap Report</h3>

            <div className="box-tools pull-right">
              <button type="button" className="btn btn-box-tool" data-widget="collapse"><i class="fa fa-calculator" aria-hidden="true"></i>              </button>
              <div className="btn-group">
                <button type="button" className="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-files-o" aria-hidden="true"></i>
                </button>
                <ul className="dropdown-menu" role="menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li className="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>
              <button type="button" className="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div className="box-body">
         <div className="row">
             <div className="col-lg-6">
               <ul className="nav  ">
                 <form id="search" role="form" style={{border:0}}>
                   <div className="col-md-6">
                     <ul className="nav nav-stacked">
                       <li><strong>From Date</strong></li>
                       <li>
                        <input className="form-control" name="from" type="date" onChange={(e)=>{
                        this.setState({from:e.target.value})
                      }}/>
                       </li>
                     </ul>
                   </div>

                   <div className="col-md-6">
                     <ul className="nav nav-stacked">
                       <li><strong>To Date</strong></li>
                       <li>
                          <input className="form-control" name="to" type="date" onChange={(e)=>{
                      this.setState({to:e.target.value});
                    }}/>
                       </li>
                     </ul>
                   </div>
      <a href="#" data-toggle="collapse" data-target="#searchForm">Advance search</a>
    <div className="collapse" id="searchForm" style={{marginButtom:10,fontSize:15}}>
    <CheckboxGroup onChange={(newSearch)=>{
      console.log(newSearch)
      this.setState({choosedSearch:newSearch});
    }}>
     {this.state.searchFields.map((search)=>{

      var element=search.name.search("radio")<0?<div><label>{search.label}</label> 
      <span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={search.label}/>Show in charts?</span>
      <input type='text' className="form-control col-md-6" name={search.label}/>
      </div>:<div style={{margin:5}}>
      <label>{search.label}</label><input type='text' className="form-control col-md-6" name={search.label}/><span className="col-md-offset-2" style={{fontSize:10}}><Checkbox value={`radio-${search.label}`}/>Show in charts?</span>
      
      </div>
      return element;
    })}
   
    </CheckboxGroup>
  {/* <button type="button" className="btn btn-primary"
   onClick={()=>{
     this.state.choosedSearch.map((single)=>{
       console.log(document.getElementsByTagName("input:text").value)
      // document.getElementsByName(single).val()

     })
   }}
   ><i className="fa fa-search"></i></button>*/}
    </div>
                   

                   <div className="col-md-3">
                     <ul className="nav nav-stacked">
                       <li>&nbsp;</li>
                       <li>
                        <a href="#" className="btn btn-sm btn-primary" onClick={()=>{
                                var self=this;
                                let arr=$("#search").serializeArray();
                                this.searchForm(arr);
                     //   var range={from:this.state.from,to:this.state.to,workflow:self.props.workflow};
                       // Meteor.call('getdataRange',range,function(err,res){
                         // console.log(res)
                         // self.setState({workflowdatas:res})
                         /// self.defaultsData();
                        //})
                      }}><i className="fa fa-search" aria-hidden="true"></i></a>
                       </li>
                     </ul>
                   </div>
                 </form>
               </ul>
             </div>
              {/*<div className="col-lg-4">
                <ul className="nav ">
                  <form id="search" role="form" style={{border:0}}>
                    <div className="col-md-6 ">
                      <ul className="nav nav-stacked">
                        <li><strong>Search By Type</strong></li>
                        <li>
                          <input type="text" className="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"/>
                        </li>
                      </ul>
                    </div>
                    <div className="col-md-6 ">
                      <ul className="nav nav-stacked">
                        <li><strong>Search By Nmae</strong></li>
                        <li>
                          <input type="text" className="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name"/>
                        </li>
                      </ul>
                    </div>
                  </form>
                </ul>
              </div>*/}
              <div className="col-lg-4">
                <ul className="nav nav-pills pull-right on-off">

                  <li className="mt8 gtext"><strong>Graphical View</strong></li>
                  <li className="active">
                    <a href="#tab_default_1" data-toggle="tab">
                      <b>OFF</b>  </a>
                  </li>
                  <li>
                    <a href="#tab_default_2" data-toggle="tab">
                      <b>ON</b> </a>
                  </li>
                </ul>
              </div>
            </div>
        </div>
         <div className="row">
            <div className="tab-content">
              <div className="tab-pane active" id="tab_default_1">

                  <div className="col-md-12">
                    <div className="holder-white">

                      <div className="table-responsive" id="myTable">
                      <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found."> 
                                        {this.state.newfields.map((single)=>{
                          return(<Tr className="myTableRow" data={single}>
                          <Td column="Action" className="actionTd"><button className="btn btn-sm btn-primary" data-toggle="modal" data-target="#myform"
                              onClick={()=>{
                              let id=single.incidentId;
                              this.setState({dataId:id});
                                let data=_.findWhere(this.props.data.workflowdatas,{_id:id});
                                data.formdata.images?this.setState({images:data.formdata.images}):this.setState({images:false})
                                let key=Object.keys(data.formdata)
                                let val=Object.values(data.formdata)
                                let c=0;
                                _.each(key,function(single){
                                  $(`#${single}`).val(val[c])
                                  c++;
                                })
                                $('.take-picture').addClass('hidden');
                              }}>Update</button></Td>
                          </Tr>)
                        })}  
                      </Table>
                      </div>
                      </div>
                      </div>
                      </div>
                       <div className="tab-pane" id="tab_default_2">
                <div className="col-md-12 mt42">
                    <Charts datas={this.state.newfields} chartFields={this.state.choosedSearch} chart={this.props.data.workflow.charts}/>
                  </div>
              </div>
                      </div>
                      </div>

       {/* <div className="col-md-10">
        <h3>Report View</h3>
         <label>from
      <input className="form-control" type="date" onChange={(e)=>{
        this.setState({from:e.target.value})
      }}/></label> &nbsp; &nbsp;
      <label>to
      <input className="form-control" type="date" onChange={(e)=>{
        this.setState({to:e.target.value});
      }}/></label>
      &nbsp;&nbsp;&nbsp;<a href="#" className="btn btn-sm btn-primary" onClick={()=>{
                var self=this;
        var range={from:this.state.from,to:this.state.to,workflow:self.props.workflow};
        Meteor.call('getdataRange',range,function(err,res){
          console.log(res)
          self.setState({workflowdatas:res})
          self.defaultsData();
        })
      }}><i className="fa fa-search" aria-hidden="true"></i></a>
        <select className="form-control col-md-6"  onClick={(e)=>{
        if(e.target.value=="default"){
         this.addFields(this.state.defield);  
        }
        else{
          $('#modalclick').trigger('click');
        }
    }}><option>View Type</option>
    <option value="default"><a href="#">default view</a></option>
      <option value="detail"><a href="#">detail view</a></option>
      </select>

       &nbsp; &nbsp;
    <br/>
    <br/> <br/> 
    <a href="#" data-toggle="collapse" data-target="#searchForm">Advance search</a>
    
    <div className="collapse" id="searchForm" style={{marginButtom:10}}>
    <h4>Tick the checkbox to include the field for charts</h4>
    <form id="mySearch">
    <CheckboxGroup onChange={(newSearch)=>{
      this.setState({choosedSearch:newSearch});
    }}>
     {this.state.searchFields.map((search)=>{

      var element=search.name.search("radio")<0?<div><label>{search.label}</label> 
      <span className="col-md-offset-2" style={{fontSize:15}}><Checkbox value={search.label}/>Show in charts?</span>
      <input type='text' className="form-control col-md-6" name={search.label}/>
      </div>:<div>
      <label><Checkbox value={search.label}/>{search.label}</label>
      </div>
      return element;
    })}
   
    </CheckboxGroup>
</form>
   <button type="button" className="btn btn-primary"
   onClick={()=>{
     this.state.choosedSearch.map((single)=>{
       console.log(document.getElementsByTagName("input:text").value)
      // document.getElementsByName(single).val()

     })
   }}
   ><i className="fa fa-search"></i></button>
    </div>
    <ul className="nav nav-tabs" role="tablist">
  <li className="nav-item">
    <a className="nav-link active" data-toggle="tab" href="#home" role="tab">Reports View</a>
  </li>
  <li className="nav-item">
    <a className="nav-link" data-toggle="tab" href="#profile" role="tab">Charts View</a>
  </li>
  </ul>
  <div className="tab-content">
  <div className="tab-pane active" id="home" role="tabpanel"> 
  <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found."> 
    {this.state.newfields.map((single)=>{
      return(<Tr className="myTableRow" data={single}>
      <Td column="Action" className="actionTd"><button className="btn btn-sm btn-primary" data-toggle="modal" data-target="#myform"
          onClick={()=>{
           let id=single.incidentId;
           this.setState({dataId:id});
            let data=_.findWhere(this.props.data.workflowdatas,{_id:id});
            data.formdata.images?this.setState({images:data.formdata.images}):this.setState({images:false})
            let key=Object.keys(data.formdata)
             let val=Object.values(data.formdata)
             let c=0;
            _.each(key,function(single){
              $(`#${single}`).val(val[c])
              c++;
            })
             $('.take-picture').addClass('hidden');
          }}>Update</button></Td>
      </Tr>)
    })}  
  </Table>
  </div>
  <div className="tab-pane" id="profile" role="tabpanel">
  <h2>Statistic Table</h2>
    <div id="chart" className="row">
    
     <h2>Barchart on basis of Date</h2>
    <BarChart width={500} height={230} data={this.state.dateData}
            margin={{top: 20, right: 30, left: 20, bottom: 5}}>
       <XAxis dataKey="date"/>
       <YAxis/>
       <CartesianGrid strokeDasharray="3 3"/>
       <Tooltip/>
       <Legend />
       <Bar dataKey="yes" stackId="a" fill="#8884d8" />
       <Bar dataKey="no" stackId="a" fill="#82ca9d" />
      </BarChart>
      
  
     <h2>Pie chart Date</h2>
       <PieChart width={800} height={400}>
        <Pie isAnimationActive={false} data={this.state.data3} cx={200} cy={200} outerRadius={80} fill="#8884d8" label/>
        <Pie data={this.state.data4} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d"/>
        <Tooltip/>
       </PieChart>
    </div>
    <hr/>
    <div className="row">
     <h2>Barchart on basis of room</h2>
      <BarChart width={600} height={250} data={this.state.roomData}
            margin={{top: 20, right: 30, left: 20, bottom: 5}}>
       <XAxis dataKey="room"/>
       <YAxis/>
       <CartesianGrid strokeDasharray="3 3"/>
       <Tooltip/>
       <Legend />
       <Bar dataKey="yes" stackId="a" fill="#8884d8" />
       <Bar dataKey="no" stackId="a" fill="#82ca9d" />
      </BarChart>
        <h2>Pie chart room no</h2>
        <PieChart width={800} height={400}>
            <Pie isAnimationActive={false} data={this.state.data1} cx={200} cy={200} outerRadius={80} fill="#8884d8" label/>
            <Pie data={this.state.data2} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d"/>
            <Tooltip/>
          </PieChart>
          
    </div>
  </div>
  </div>
   

      </div>
      <div className="col-md-2">
      <select className="form-control" onChange={(e)=>{
       
        var self=this;
        this.setState({type:e.target.value})
       Meteor.call('getdataperiod',e.target.value,function(err,res){
         if(!err){
            self.setState({workflowdatas:res})
            self.table_fields();
         }
       })
      }}>
      <option>choose time view</option>
      <option value="daily">Daily view</option>
      <option value="weekly">weekly view</option>
      <option value="monthly">Monthly view</option>
      
      </select>

       <table className="table table-bordered table-hover table-striped margin-top20" style={{fontSize:12}}>
    <thead>
    <tr>
    {this.state.t!=''?this.state.t.field.map((a)=>{
        return(<th>{a}</th>)
    }):<th></th>}   
      </tr>
    </thead>
    <tbody>
    {this.state.t!=''?this.state.t.countData.map((row)=>{
      
      return(<tr>{row.map((r)=>{
        return(<td>{r}</td>)
      })}</tr>)
    }):<tr><td></td></tr>}
    </tbody>
    </table>
      </div>
      <div>
      </div>
     
      </div>*/}
      {/*FormData*/}
       <div className="modal fade" id="myform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel"></h4>
      </div>
      <div className="modal-body">
      <div className="col-md-12">
          <form id="showform">
          
          </form> 
      
       <div className="images">
       <h2>Uploaded Images</h2>
           {this.state.images.map((image)=>{
          return(<div className="col-xs-6 col-md-3">
    <a href="#" className="thumbnail">
      <img src={image} alt="..."/>
    </a>
  </div>)  })}
          </div>
           </div>
           <hr/>
          <div>
          <br/>
       
          <label>Remark</label>
          <textarea type="textarea" className="form-control"  id="remark"></textarea>
          <a className="btn btn-primary" id="addRemark">Submit</a>
          </div>
          
          
          
      </div>
      <div className="modal-footer">
             </div>
    </div>
    </div></div>

      {/*detail view*/}
      <button id="modalclick" data-toggle="modal" data-target="#mymodals" className="hidden"></button>
         <div className="modal fade" id="mymodals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" id="myModalLabel">Choose other fields</h4>
      </div>
      <div className="modal-body">
        <CheckboxGroup name="fruits" value='' onChange={(fields)=>{
          console.log(this.state.defield);
          let d=this.state.defield;
          let newf=d.concat(fields);
          console.log(newf)
           this.addFields(newf);    
         // this.setState({<new></new>fields:newf});
        }}>
        <ul style={{listStyleType:"none"}}>
        {this.state.otherfields.map((single)=>{
          return(<li><label><Checkbox value={single} Checked/>{single}</label></li>)
        })}
       
        </ul>
        </CheckboxGroup>
      </div>
      <div className="modal-footer">
             </div>
    </div>
  </div>
</div>
      </div>
    </div>
    </div>  
  	)
  }
}