import React ,{Component} from 'react'
import AddClientUser from '../../../container/addClientUser.js'
import crudClass from '../../common/crudClass.js'
//import { Table,SearchColumns, search,sort} from 'reactabular';
import orderBy from 'lodash/orderBy';
import Paginate from '../../common/paginator.jsx'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ManageClientUser extends Component {
  constructor(props) {
   super(props)
   
  }
 
 
 render(){
   
    return(<div>
  <section className="content">
  <div className="col-md-10">
      <div className="col-md-10 col-md-offset-1">
        <h1 className="title">Manage Client Users</h1>
           <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No User found." itemsPerPage={30} sortable={true}>
            {this.props.data.users.map(function(row) {
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Name">{row.name}</Td>
                            <Td column="Phone">{row.contact}</Td>
                            <Td column="Email">{row.email}</Td>
                            <Td column="Address">{row.address}</Td>
                            
                            <Td column="Action">
                              <div className="">
                                <ReactTooltip id='edit'  type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                              
                                <a href="#" data-tip data-for="edit"  data-toggle="modal" data-target={`#${row._id}`} className="round-primary edit"><i 
                                className="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;
                                <div className="modal fade"   id={`${row._id}`} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">  
                                <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title">Modal title</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
 <AddClientUser edit="true" clientUser={row} />

</div>
</div>
</div>
</div>
                              <ReactTooltip id='delete'  type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                              &nbsp;&nbsp;&nbsp;
                                <a href="#" className="round-danger" data-tip data-for="delete" id={row._id} onClick={(e)=>{
                                //  console.log(e.target.id)
                                  let obj=new crudClass()
                                  obj.delete('deleteClientUser',e.target.id)
                                }}><i className="fa fa-trash-o" id={row._id}></i></a></div>
                            </Td>
                        </Tr>
                    )
                })}
        </Table>
        
      </div></div></section></div>
    )
  }
}
