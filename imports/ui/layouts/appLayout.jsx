import React,{Component} from 'react';
import Header from '../container/clientUserHeader.js'
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';
import ClientUserSidebar from '../container/clientUserSidebar.js'
export default AppLayout=(props)=> {
    return(<div className="hold-transition skin-blue sidebar-mini">
  <div className="wrapper">
  <Header/>
  <ClientUserSidebar/>
  <div className="content-wrapper ">
    <section className="content-header">
    User App
    </section>
    <section className="content">
    {props.content}
    </section>
      </div>

  </div>
</div>
)
}
