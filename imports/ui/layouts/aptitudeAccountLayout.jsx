import React ,{Component} from 'react'
import AdminLogin from '../components/accounts/login/adminlogin.jsx'
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';

export default class AptitudeLogin extends Component {
  constructor(props) {
    super(props);
  }
// <div className="admin_hed_top">

//       <div className="mid_container">
//         <div className="logoin">Login</div>
//       </div>
//     </div>
  render(){
    return(
    <section className="aptitude-login-layout">
    {this.props.content?this.props.content:<div>

      <AdminLogin/><Alert stack={{limit: 3}}/></div>}
      
    </section>

  );

  }
}