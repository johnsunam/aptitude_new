import React,{Component} from 'react'
import Header from '../container/header.js';
import Footer from '../components/common/footer.jsx'
import SiderBar from '../components/common/siderbar.jsx';
import AdminLogin from '../components/accounts/login/adminlogin.jsx'
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';

export default MainLayout=(props)=> {
    return(
      <div>
        <Header/>

        <div className="no_pad clearfix">
        <div className="clearfix overflow">
        <div className="row">
        <div className="col-md-2" style={{height: 800}}>
         <SiderBar/>
        </div>
        {props.content}
        </div>
       


        </div>
        </div>

        <Footer/>
        <Alert stack={{limit: 3}}/>
    </div>
  );
}
