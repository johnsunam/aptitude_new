import {FormDataDb} from './collection/formdata.collection.js';
import {WorkflowDataDb} from'./collection/workflowData.collection.js'
import moment from '../../../public/lib/moment-2.13.0'
Meteor.methods({
  'addFormData':function(data){

    FormDataDb.insert(data)
  },
  'addWorkflowData':function(data){
    WorkflowDataDb.insert(data)
  },
  'updateFormData':function(data){
    FormDataDb.update({_id:data.id},{$set:{formdata:data.formdata}})
  },
  updateWorkflowData:function(data){
     WorkflowDataDb.update({_id:data.id},{$set:{formdata:data.formdata}})
  },
  'getdataperiod':function(type){
    if(type=='daily'){
      let date=moment().startOf('day').toDate()
      let datas=WorkflowDataDb.find({createdAt:{$gt:date}}).fetch();
      return datas;
    }
    if(type=='weekly'){
      let from=moment().startOf('week').toDate()
      let to=moment().toDate();
      let datas=WorkflowDataDb.find({createdAt:{$gt:from,$lt:to}}).fetch();
      return datas;
    }
    if(type=='monthly'){
      let from=moment().startOf('month').toDate()
      let to=moment().toDate();
      console.log(from,to)
      let datas=WorkflowDataDb.find({createdAt:{$gt:from,$lt:to}}).fetch();
      return datas;
    }
  },

  "getdataRange":function(range){
    console.log(range)
    let from=moment(range.from).toDate();
    let to=moment(range.to).toDate();
    let datas=WorkflowDataDb.find({workflow:range.workflow,createdAt:{$gte:from,$lte:to}}).fetch();
    console.log(datas)
    return datas;
  }
})
