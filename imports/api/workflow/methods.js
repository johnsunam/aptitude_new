import {WorkflowDb} from "./collection/workflow.collection.js"

Meteor.methods({
  'addWorkFlow':function(record){
    return WorkflowDb.insert(record);
  },
  "saveFlowchart":function (data) {
   WorkflowDb.update({_id:data.workflow},{$set:{charts:data.charts,flowchart:data.flowchartData,action:data.action,email:data.email}})
  },
  'deleteWorkFlow':function(id){
    WorkflowDb.remove({_id:id});
  },
  'editWorkFlow':function(record){
  return  WorkflowDb.update({_id:record.id},{$set:record.data})
},

})
